using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using ViveSR.anipal.Eye;

public class FrameByFrame_DataRecorder : MonoBehaviour
{
    //------------------------------------------------------------------- Variables declaration -------------------------------------------------------------------
    private static readonly string dataSeparator = ",";
    private static readonly string[] dataHeaders = new string[]
    {
        "Launch counter",           //Number of launch
        "Elapsed time",             //Elapsed time since the start of the current launch
        "Absolute time",            //Current timetable
        "HMD position X",           //Current position of the HMD Camera - X component
        "HMD position Y",           //Current position of the HMD Camera - Y component
        "HMD position Z",           //Current position of the HMD Camera - Z component  
        "HMD rotation X",           //Current rotation of the HMD Camera - X component
        "HMD rotation Y",           //Current rotation of the HMD Camera - Y component
        "HMD rotation Z",           //Current rotation of the HMD Camera - Z component
        "Gaze X",                   //Vector indicating the user's gaze direction - X component
        "Gaze Y",                   //Vector indicating the user's gaze direction - Y component
        "Gaze Z",                   //Vector indicating the user's gaze direction - Z component
        "Focused object",           //Name of the object on which the user is focusing his gaze
        "Left eye openness",        //How much the left eye is open - [0:1] = [min_openness:max_openness]
        "Right eye openness",       //How much the right eye is open - [0:1] = [min_openness:max_openness]
        "Blink flag",               //Boolean indicating whether the eyes are closed or not - True = Close / False = Open
        "Left pupil diameter",      //Left pupil diameter in mm
        "Right pupil diameter",     //Right pupil diameter in mm
        "Left pupil position X",    //
        "Left pupil position Y",    //
        "Right pupil position X",
        "Right pupil position Y",
        "Ball position X",
        "Ball position Y",
        "Ball position Z",
        "Ball velocity X",
        "Ball velocity Y",
        "Ball velocity Z",
        "Debug ball position X",
        "Debug ball position Y",
        "Debug ball position Z",
        "Debug ball velocity X",
        "Debug ball velocity Y",
        "Debug ball velocity Z",
        "Hand position X",
        "Hand position Y",
        "Hand position Z",
        "Hand rotation X",
        "Hand rotation Y",
        "Hand rotation Z",
    };

    public enum RecordingState { StartRecording, ContinueRecording, NotRecording }
    public RecordingState recordingState;

    private float elapsedTime = 0f;
    readonly List<string> dataList = new();

    public SRanipal_GazeRaySample SRanipalGaze;
    public Camera camera;
    [SerializeField]
    private EyeState eyeState;

    [SerializeField]
    private GameObject ball, debugBall, hand;




    void Start()
    {
        System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
        customCulture.NumberFormat.NumberDecimalSeparator = ".";
        System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

        recordingState = RecordingState.NotRecording;
    }

    private void FixedUpdate()
    {
        elapsedTime += Time.deltaTime;

        switch (recordingState)
        {
            case RecordingState.StartRecording:
                recordingState = RecordingState.ContinueRecording;
                LogSave();
                break;
            case RecordingState.ContinueRecording:
                recordingState = RecordingState.ContinueRecording;
                LogSave();
                break;
        }      
    }

    private void LogSave()
    {
        //Launch Counter
        dataList.Add(StaticValues.launchCounter.ToString());

        //Time Data
        dataList.Add(elapsedTime.ToString());
        dataList.Add(DateTime.Now.Hour.ToString() + ":" +
                          DateTime.Now.Minute.ToString() + ":" +
                          DateTime.Now.Second.ToString() + ":" +
                          DateTime.Now.Millisecond.ToString());

        //HMD Data
        dataList.Add(Math.Round(camera.transform.position.x, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(camera.transform.position.y, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(camera.transform.position.z, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(camera.transform.rotation.eulerAngles.x, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(camera.transform.rotation.eulerAngles.y, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(camera.transform.rotation.eulerAngles.z, 4, MidpointRounding.AwayFromZero).ToString());

        //Eye Tracking Data
        if(StaticValues.eyeChoice == "On")
        {
            dataList.Add(Math.Round(SRanipalGaze.GetGaze().x, 4, MidpointRounding.AwayFromZero).ToString());
            dataList.Add(Math.Round(SRanipalGaze.GetGaze().y, 4, MidpointRounding.AwayFromZero).ToString());
            dataList.Add(Math.Round(SRanipalGaze.GetGaze().z, 4, MidpointRounding.AwayFromZero).ToString());
            dataList.Add(eyeState.focusObject);
            dataList.Add(eyeState.leftOpenness.ToString());
            dataList.Add(eyeState.rightOpenness.ToString());
            dataList.Add(eyeState.blinkFlag.ToString());
            dataList.Add(eyeState.leftPupilDiameter.ToString());
            dataList.Add(eyeState.rightPupilDiameter.ToString());
            dataList.Add(eyeState.leftPupilPosition.x.ToString());
            dataList.Add(eyeState.leftPupilPosition.y.ToString());
            dataList.Add(eyeState.rightPupilPosition.x.ToString());
            dataList.Add(eyeState.rightPupilPosition.y.ToString());
        }
        else
        {
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
        }



        //Ball Data
        dataList.Add(Math.Round(ball.transform.position.x, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(ball.transform.position.y, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(ball.transform.position.z, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(ball.GetComponent<Rigidbody>().velocity.x, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(ball.GetComponent<Rigidbody>().velocity.y, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(ball.GetComponent<Rigidbody>().velocity.z, 4, MidpointRounding.AwayFromZero).ToString());

        dataList.Add(Math.Round(debugBall.transform.position.x, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(debugBall.transform.position.y, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(debugBall.transform.position.z, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(debugBall.GetComponent<Rigidbody>().velocity.x, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(debugBall.GetComponent<Rigidbody>().velocity.y, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(debugBall.GetComponent<Rigidbody>().velocity.z, 4, MidpointRounding.AwayFromZero).ToString());
        //Debug Ball Data
        /*
        if(StaticValues.enhancementMode == "None")
        {
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
            dataList.Add(0.ToString());
        }
        else
        {
            dataList.Add(Math.Round(debugBall.transform.position.x, 4, MidpointRounding.AwayFromZero).ToString());
            dataList.Add(Math.Round(debugBall.transform.position.y, 4, MidpointRounding.AwayFromZero).ToString());
            dataList.Add(Math.Round(debugBall.transform.position.z, 4, MidpointRounding.AwayFromZero).ToString());
            dataList.Add(Math.Round(debugBall.GetComponent<Rigidbody>().velocity.x, 4, MidpointRounding.AwayFromZero).ToString());
            dataList.Add(Math.Round(debugBall.GetComponent<Rigidbody>().velocity.y, 4, MidpointRounding.AwayFromZero).ToString());
            dataList.Add(Math.Round(debugBall.GetComponent<Rigidbody>().velocity.z, 4, MidpointRounding.AwayFromZero).ToString());
        }*/


        //Hand Data
        dataList.Add(Math.Round(hand.transform.position.x, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(hand.transform.position.y, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(hand.transform.position.z, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(hand.transform.rotation.eulerAngles.x, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(hand.transform.rotation.eulerAngles.y, 4, MidpointRounding.AwayFromZero).ToString());
        dataList.Add(Math.Round(hand.transform.rotation.eulerAngles.z, 4, MidpointRounding.AwayFromZero).ToString());

        AppendData(dataList.ToArray());
        dataList.Clear();
    }

    private void VerifyDirectory()
    {
        string dir = GetDirectoryPath();
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
    }

    private void VerifyFile()
    {
        string file = GetFilePath();
        if (!File.Exists(file))
        {
            CreateDataFile();
        }
    }

    private string GetDirectoryPath()
    {
            return Application.dataPath + StaticValues.pathLaunchByLaunch;
    }

    private string GetFilePath()
    {
            return GetDirectoryPath() + "/" + "FBF_" + StaticValues.userName + ".csv";
    }

    public void AppendData(string[] dataString)
    {
        VerifyDirectory();
        VerifyFile();

        TextWriter tw = new StreamWriter(GetFilePath(), true);

        string stringToWrite = "";
        foreach (string str in dataString)
        {
            if (stringToWrite != "")
            {
                stringToWrite += dataSeparator;
            }
            stringToWrite += str;
        }

        tw.WriteLine(stringToWrite);
        tw.Close();
    }

    public void CreateDataFile()
    {
        VerifyDirectory();
        string stringToWrite = "";
        foreach (string str in dataHeaders)
        {
            if (stringToWrite != "")
            {
                stringToWrite += dataSeparator;
            }
            stringToWrite += str;
        }
        _ = new FileInfo(GetFilePath());
        TextWriter tw = new StreamWriter(GetFilePath(), true);
        tw.WriteLine(stringToWrite);
        tw.Close();
    }

    public void StartRecordingData()
    {
        
        if(recordingState == RecordingState.NotRecording)
        {
            CreateDataFile();
            recordingState = RecordingState.StartRecording;
            Debug.Log("REC...");
        }
    }

    public void StopRecordingData()
    {
        if (recordingState == RecordingState.ContinueRecording)
        {
            recordingState = RecordingState.NotRecording;
            Debug.Log("Stop REC!");
        }
    }

    

}
