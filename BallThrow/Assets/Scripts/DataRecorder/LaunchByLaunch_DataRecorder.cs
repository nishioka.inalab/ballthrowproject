using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LaunchByLaunch_DataRecorder : MonoBehaviour
{
    private static readonly string dataSeparator = ",";
    private static readonly string[] dataHeaders = new string[]
    {
        "Launch counter",
        "Elapsed time",
        "Absolute time",
        "Enhancement mode",
        "Target mode",
        "Support rate",
        "RedBall initial position X",
        "RedBall initial position Y",
        "RedBall initial position Z",
        "Target position X",
        "Target position Y",
        "Target position Z",
        "Target rotation X",
        "Target rotation Y",
        "Target rotation Z",
        "Real velocity X",
        "Real velocity Y",
        "Real velocity Z",
        "Real velocity magnitude",
        "Velocity force X",
        "Velocity force Y",
        "Velocity force Z",
        "Direction force X",
        "Direction force Y",
        "Direction force Z",
        "Enhanced velocity X",
        "Enhanced velocity Y",
        "Enhanced velocity Z",
        "Enhanced velocity magnitude",
        "Angular velocity X",
        "Angular velocity Y",
        "Angular velocity Z",
        "Direction error",
        "Enhanced direction error",
        "Target hit",
        "Error",
        "Drop point X",
        "Drop point Y"
    };
    private readonly List<string> dataList = new();


    void Start()
    {
        System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
        customCulture.NumberFormat.NumberDecimalSeparator = ".";
        System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
    }
    private void VerifyDirectory()
    {
        string dir = GetDirectoryPath();
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
    }

    private void VerifyFile()
    {
        string file = GetFilePath();
        if (!File.Exists(file))
        {
            CreateDataFile();
        }
    }

    private string GetDirectoryPath()
    {
        return Application.dataPath + UserValues.pathLaunchByLaunch;
    }

    private string GetFilePath()
    {

        return GetDirectoryPath() + "/" + "LBL_" + UserValues.userName + ".csv"; ;
    }

    public void AppendData(string[] dataString)
    {
        VerifyDirectory();
        VerifyFile();

        TextWriter tw = new StreamWriter(GetFilePath(), true);

        string stringToWrite = "";
        foreach (string str in dataString)
        {
            if (stringToWrite != "")
            {
                stringToWrite += dataSeparator;
            }
            stringToWrite += str;
        }

        tw.WriteLine(stringToWrite);
        tw.Close();
    }

    public void CreateDataFile()
    {
        VerifyDirectory();
        string stringToWrite = "";
        foreach (string str in dataHeaders)
        {
            if (stringToWrite != "")
            {
                stringToWrite += dataSeparator;
            }
            stringToWrite += str;
        }
        _ = new FileInfo(GetFilePath());
        TextWriter tw = new StreamWriter(GetFilePath(), true);
        tw.WriteLine(stringToWrite);
        tw.Close();
    }

}
