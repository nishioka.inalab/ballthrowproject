using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class BallManager : MonoBehaviour
{
    [SerializeField]
    private GameObject target;
    [SerializeField]
    private GameObject wall;
    [SerializeField]
    private UIManager uiManager;
    [SerializeField]
    private AudioManager audioManager;

    //SerializeField]
    //private SupportRateManager supportRateManager;
    private int previousLaunch = 0;
    private static readonly string dataSeparator = ",";
    private readonly List<string> dataList = new();
    private static readonly string[] dataHeaders = new string[]
    {
        "Launch number",
        "Target hit",
        "Error",
        "Drop point X",
        "Drop point Y"
    };

    void Start()
    {
        StaticValues.ballIsFlying = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (StaticValues.launchCounter == 10 && StaticValues.phaseNr == 0)
        {
            StaticValues.enableGrab = false;
            uiManager.EnableTrainingCanvas();
        }
        /*if (StaticValues.launchCounter == 30 && (StaticValues.phaseNr == 1 || StaticValues.phaseNr == 3))
        {
            StaticValues.enableGrab = false;
            uiManager.EnableTrainingCanvas();
        }*/

        if (StaticValues.launchCounter == 50 && (StaticValues.phaseNr == 3 || StaticValues.phaseNr == 4))
        {
            StaticValues.enableGrab = false;
            uiManager.EnableTrainingCanvas();
        }

        /*if (StaticValues.launchCounter == 60 && StaticValues.phaseNr == 2)
        {
            StaticValues.enableGrab = false;
            uiManager.EnableTrainingCanvas();
        }*/

        if (StaticValues.ballIsFlying)
        {
            StaticValues.ballIsFlying = false;
            if (StaticValues.launchCounter == (previousLaunch + 1))
            {
                if(StaticValues.phaseNr == 3 || StaticValues.phaseNr == 4)
                {
                    StaticValues.enableGrab = false;
                }
                
                previousLaunch++;
                dataList.Add(StaticValues.launchCounter.ToString());
                float currentError;
                Vector3 dropPoint = Vector3.zero;
                Vector3 targetPoint = Vector3.zero;
                if (StaticValues.targetChoice == "Ground")
                {
                    if (collision.gameObject == target)
                    {                      
                        audioManager.PlayHitTargetSound();
                        dataList.Add(true.ToString());

                        /*if (StaticValues.enhancementMode == "SupportRate" && StaticValues.adjustMode == "Dynamic")
                        {
                            supportRateManager.CountSuccess();
                        }*/
                    }
                    else
                    {
                        audioManager.PlayHitGrassSound();
                        StaticValues.floorHit = true;
                        dataList.Add(false.ToString());

                        /*if (StaticValues.enhancementMode == "SupportRate" && StaticValues.adjustMode == "Dynamic")
                        {
                            supportRateManager.CountFailure();
                        }*/
                    }

                    dropPoint.Set(collision.contacts[0].point.x, 0, collision.contacts[0].point.z);
                    targetPoint.Set(target.transform.position.x, 0, target.transform.position.z);
                    currentError = Vector3.Distance(dropPoint, targetPoint);

                    dataList.Add(currentError.ToString());
                    Vector2 scatterCoordinates = GetScatterCoordinates(dropPoint, targetPoint);
                    dataList.Add(scatterCoordinates.x.ToString());
                    dataList.Add(scatterCoordinates.y.ToString());
                    
                }
                else if (StaticValues.targetChoice == "Air")
                {
                    if (collision.gameObject == target)
                    {
                        audioManager.PlayHitTargetSound();
                        dataList.Add(true.ToString());
                        /*if (StaticValues.enhancementMode == "SupportRate" && StaticValues.adjustMode == "Dynamic")
                        {
                            supportRateManager.CountSuccess();
                        }*/

                        dropPoint.Set(0, collision.contacts[0].point.y, 0);
                        targetPoint.Set(0, target.transform.position.y, 0);
                        currentError = Vector3.Distance(dropPoint, targetPoint);

                        dataList.Add(currentError.ToString());
                    }
                    else
                    {
                        audioManager.PlayHitGrassSound();
                        StaticValues.floorHit = true;
                        dataList.Add(false.ToString());

                        /*if (StaticValues.enhancementMode == "SupportRate" && StaticValues.adjustMode == "Dynamic")
                        {
                            supportRateManager.CountFailure();
                        }*/

                        dropPoint = collision.contacts[0].point;
                        targetPoint = target.transform.position;
                        currentError = Vector3.Distance(dropPoint, targetPoint);

                        if (currentError < 0.075)
                        {
                            currentError = 0f;
                        }

                        dataList.Add(currentError.ToString());
                    }

                    dataList.Add("0");
                    dataList.Add("0");
                }
                AppendData(dataList.ToArray());
                dataList.Clear();
            }
        }
    }

    private Vector2 GetScatterCoordinates(Vector3 dropPoint, Vector3 targetPoint)
    {
        float xCoord = dropPoint.x - targetPoint.x;
        float yCoord = dropPoint.z - targetPoint.z;
        return new Vector2(xCoord, yCoord);
    }
    private void VerifyDirectory()
    {
        string dir = GetDirectoryPath();
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
    }

    private void VerifyFile()
    {
        string file = GetFilePath();
        if (!File.Exists(file))
        {
            CreateDataFile();
        }
    }

    private string GetDirectoryPath()
    {
            return Application.dataPath + StaticValues.pathGeneral;
    }

    private string GetFilePath()
    {
            return GetDirectoryPath() + "/BALL_" + StaticValues.userName + "_" + StaticValues.phaseNr + ".csv";
    }

    public void AppendData(string[] dataString)
    {
        VerifyDirectory();
        VerifyFile();

        TextWriter tw = new StreamWriter(GetFilePath(), true);

        string stringToWrite = "";
        foreach (string str in dataString)
        {
            if (stringToWrite != "")
            {
                stringToWrite += dataSeparator;
            }
            stringToWrite += str;
        }

        tw.WriteLine(stringToWrite);
        tw.Close();
    }

    public void CreateDataFile()
    {
        VerifyDirectory();
        string stringToWrite = "";
        foreach (string str in dataHeaders)
        {
            if (stringToWrite != "")
            {
                stringToWrite += dataSeparator;
            }
            stringToWrite += str;
        }
        _ = new FileInfo(GetFilePath());
        TextWriter tw = new StreamWriter(GetFilePath(), true);
        tw.WriteLine(stringToWrite);
        tw.Close();
    }
}
