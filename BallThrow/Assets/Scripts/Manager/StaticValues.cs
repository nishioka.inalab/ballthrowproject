using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticValues : MonoBehaviour
{
    public static bool trainingMode { get; set; }
    public static string hand { get; set; }
    public static bool debugMode { get; set; }
    public static string userName { get; set; }
    public static string targetChoice { get; set; }
    public static string eyeChoice { get; set; }
    public static bool moveTarget { get; set; }
    public static int phaseNr { get; set; }
    public static int launchCounter { get; set; }
    public static string pathGeneral { get; set; }
    public static string pathLaunchByLaunch { get; set; }
    public static int supportRate { get; set; }
    public static string enhancementMode { get; set; }
    public static string adjustMode { get; set; }
    public static bool ballIsFlying { get; set; }
    public static bool ballIsGrabbed { get; set; }
    public static bool floorHit { get; set; }
    public static bool debugBallIsFlying { get; set; }
    public static bool debugBallIsGrabbed { get; set; }
    public static bool debugFloorHit { get; set; }
    public static bool enableGrab { get; set; }
}
