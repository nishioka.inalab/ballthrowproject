using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

public class SupportRateManager : MonoBehaviour
{
    private enum SupportRateAdjustMode { Manual, Constant, Dynamic }

    [SerializeField]
    private Slider supportRateSlider;
    [SerializeField]
    UIManager uiManager;
    private void Start()
    {
        switch(StaticValues.phaseNr)
        {
            case 0: StaticValues.supportRate = 0; uiManager.UpdateSliderValue(); break;
            case 1: StaticValues.supportRate = 19; uiManager.UpdateSliderValue(); break;
            case 2: StaticValues.supportRate = 51; uiManager.UpdateSliderValue(); break;
            case 3: StaticValues.supportRate = 19; uiManager.UpdateSliderValue(); break;
            case 4: StaticValues.supportRate = 0; uiManager.UpdateSliderValue(); break;
        }
    }

    public void AdjustSupportRate()
    {
        switch (StaticValues.phaseNr)
        {
            case 0: break;
            case 1: 
                if(StaticValues.supportRate < 100) 
                {
                    StaticValues.supportRate++; uiManager.UpdateSliderValue(); 
                }
                break;
            case 2:
                if (StaticValues.supportRate > 0)
                {
                    StaticValues.supportRate--; uiManager.UpdateSliderValue();
                }
                break;
            case 3: 
                switch (StaticValues.launchCounter)
                {
                    case (<= 11): 
                        StaticValues.supportRate++;
                        if (StaticValues.supportRate == 30)
                        {
                            StaticValues.supportRate = 20;
                        }
                        break;
                    case (<= 21):
                        StaticValues.supportRate += 2;
                        if (StaticValues.supportRate == 40)
                        {
                            StaticValues.supportRate = 20;
                        }
                        break;
                    case (<= 31):
                        StaticValues.supportRate += 3;
                        if (StaticValues.supportRate == 50)
                        {
                            StaticValues.supportRate = 20;
                        }
                        break;
                    case (<= 41): StaticValues.supportRate += 4;
                        if (StaticValues.supportRate == 60)
                        {
                            StaticValues.supportRate = 20;
                        }
                        break;
                    case (<= 51): StaticValues.supportRate += 5;
                        if (StaticValues.supportRate == 70)
                        {
                            StaticValues.supportRate = 20;
                        }
                        break;
                };
                uiManager.UpdateSliderValue();
                break;
            case 4: break;
        }
    }
}
