using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DebugBallManager : MonoBehaviour
{
    [SerializeField]
    private GameObject target;
    [SerializeField]
    private GameObject wall;
    //SerializeField]
    //private SupportRateManager supportRateManager;
    private int previousLaunch = 0;
    private static readonly string dataSeparator = ",";
    private readonly List<string> dataList = new();
    private static readonly string[] dataHeaders = new string[]
    {
        "Launch number",
        "Target hit",
        "Error",
        "Drop point X",
        "Drop point Y"
    };

    void Start()
    {
        StaticValues.debugBallIsFlying = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (StaticValues.debugBallIsFlying)
        {
            StaticValues.debugBallIsFlying = false;
            if (StaticValues.launchCounter == (previousLaunch + 1))
            {
                previousLaunch++;
                dataList.Add(StaticValues.launchCounter.ToString());
                float currentError;
                Vector3 dropPoint = Vector3.zero;
                Vector3 targetPoint = Vector3.zero;
                if (StaticValues.targetChoice == "Ground")
                {
                    
                    if (collision.gameObject == target)
                    {
                        dataList.Add(true.ToString());

                        /*if (StaticValues.enhancementMode == "SupportRate" && StaticValues.adjustMode == "Dynamic")
                        {
                            supportRateManager.CountSuccess();
                        }*/
                    }
                    else
                    {
                        StaticValues.debugFloorHit = true;
                        dataList.Add(false.ToString());

                        /*if (StaticValues.enhancementMode == "SupportRate" && StaticValues.adjustMode == "Dynamic")
                        {
                            supportRateManager.CountFailure();
                        }*/
                    }

                    dropPoint.Set(collision.contacts[0].point.x, 0, collision.contacts[0].point.z);
                    targetPoint.Set(target.transform.position.x, 0, target.transform.position.z);
                    currentError = Vector3.Distance(dropPoint, targetPoint);

                    dataList.Add(currentError.ToString());
                    Vector2 scatterCoordinates = GetScatterCoordinates(dropPoint, targetPoint);
                    dataList.Add(scatterCoordinates.x.ToString());
                    dataList.Add(scatterCoordinates.y.ToString());
                }
                else if (StaticValues.targetChoice == "Air")
                {
                    if (collision.gameObject == target)
                    {
                        dataList.Add(true.ToString());
                        /*if (StaticValues.enhancementMode == "SupportRate" && StaticValues.adjustMode == "Dynamic")
                        {
                            supportRateManager.CountSuccess();
                        }*/

                        dropPoint.Set(0, collision.contacts[0].point.y, 0);
                        targetPoint.Set(0, target.transform.position.y, 0);
                        currentError = Vector3.Distance(dropPoint, targetPoint);


                        if (currentError < 0.075)
                        {
                            currentError = 0f;
                        }

                        dataList.Add(currentError.ToString());
                    }
                    else
                    {
                        StaticValues.debugFloorHit = true;
                        dataList.Add(false.ToString());

                        /*if (StaticValues.enhancementMode == "SupportRate" && StaticValues.adjustMode == "Dynamic")
                        {
                            supportRateManager.CountFailure();
                        }*/

                        dropPoint = collision.contacts[0].point;
                        targetPoint = target.transform.position;
                        currentError = Vector3.Distance(dropPoint, targetPoint);

                        if (currentError < 0.075)
                        {
                            currentError = 0f;
                        }

                        dataList.Add(currentError.ToString());
                    }

                    dataList.Add("0");
                    dataList.Add("0");
                }
                AppendData(dataList.ToArray());
                dataList.Clear();
            }
        }
    }

    private Vector2 GetScatterCoordinates(Vector3 dropPoint, Vector3 targetPoint)
    {
        float xCoord = dropPoint.x - targetPoint.x;
        float yCoord = dropPoint.z - targetPoint.z;
        return new Vector2(xCoord, yCoord);
    }
    private void VerifyDirectory()
    {
        string dir = GetDirectoryPath();
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
    }

    private void VerifyFile()
    {
        string file = GetFilePath();
        if (!File.Exists(file))
        {
            CreateDataFile();
        }
    }

    private string GetDirectoryPath()
    {
            return Application.dataPath + StaticValues.pathGeneral;
    }

    private string GetFilePath()
    {
            return GetDirectoryPath() + "/DEBUGBALL_" + StaticValues.userName + "_" + StaticValues.phaseNr + ".csv";
    }

    public void AppendData(string[] dataString)
    {
        VerifyDirectory();
        VerifyFile();

        TextWriter tw = new StreamWriter(GetFilePath(), true);

        string stringToWrite = "";
        foreach (string str in dataString)
        {
            if (stringToWrite != "")
            {
                stringToWrite += dataSeparator;
            }
            stringToWrite += str;
        }

        tw.WriteLine(stringToWrite);
        tw.Close();
    }

    public void CreateDataFile()
    {
        VerifyDirectory();
        string stringToWrite = "";
        foreach (string str in dataHeaders)
        {
            if (stringToWrite != "")
            {
                stringToWrite += dataSeparator;
            }
            stringToWrite += str;
        }
        _ = new FileInfo(GetFilePath());
        TextWriter tw = new StreamWriter(GetFilePath(), true);
        tw.WriteLine(stringToWrite);
        tw.Close();
    }
}
