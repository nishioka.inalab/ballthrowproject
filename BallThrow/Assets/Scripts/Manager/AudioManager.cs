using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioClip hitTargetSound, hitGrassSound, grabSound;

    public void PlayHitTargetSound()
    {
        this.GetComponent<AudioSource>().PlayOneShot(hitTargetSound);
    }

    public void PlayHitGrassSound()
    {
        this.GetComponent<AudioSource>().PlayOneShot(hitGrassSound);
    }
    public void PlayGrabSound()
    {
        this.GetComponent<AudioSource>().PlayOneShot(grabSound);
    }
}
