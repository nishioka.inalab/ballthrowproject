using UnityEngine;
using Valve.VR.InteractionSystem;
using System.Collections.Generic;
using System;
using System.IO;

public class ThrowManager : MonoBehaviour
{

    [SerializeField]
    private FrameByFrame_DataRecorder fbf_dataRecorder;
    [SerializeField]
    private EnhancementManager enhancementManager;
    [SerializeField]
    private AudioManager audioManager;
    [SerializeField]
    private UIManager uIManager;
    [SerializeField]
    private Material M_SE_neutral, M_SE_positive, M_SE_negative;
    [SerializeField]
    private SupportRateManager supportRateManager;

    private float angle, oldAngle, absAngle = 0f;
    private float velocity, oldVelocity = 0f;
    private float elapsedTime = 0f;

    //Readonly values
    private readonly float adjustValueAngle = 0.75f;
    private readonly float shoulderLength = 0.5f;

    [SerializeField]
    private GameObject hand, ball, debugBall, target, shoulder, rightHandTrackerModel, leftHandTrackerModel, rightHandControllerModel, leftHandControllerModel, SE_indicator;
    private GameObject objectInHand, collidingObject;
    private bool SE_setted = false;

    //Data dictionary
    private Dictionary<string, string> csvDict;

    //CSV path
    private string CSVpath = "";

    // Start is called before the first frame update
    void Start()
    {
        if(StaticValues.hand == "Right-handed")
        {
            rightHandTrackerModel.SetActive(true);
            leftHandControllerModel.SetActive(true);

            rightHandControllerModel.SetActive(false);
            leftHandTrackerModel.SetActive(false);
            
        }
        else if(StaticValues.hand == "Left-handed")
        {
            leftHandTrackerModel.SetActive(true);
            rightHandControllerModel.SetActive(true);

            leftHandControllerModel.SetActive(false);
            rightHandTrackerModel.SetActive(false);
        }

        CSVpath = Application.dataPath + StaticValues.pathGeneral + "/GENERAL_" + StaticValues.userName + "_" + StaticValues.phaseNr + ".csv";
        
        StaticValues.enableGrab = true;

        StaticValues.ballIsFlying = false;
        StaticValues.ballIsGrabbed = false;
        StaticValues.floorHit = false;

        StaticValues.debugBallIsFlying = false;
        StaticValues.debugBallIsGrabbed = false;
        StaticValues.debugFloorHit = false;


        if (StaticValues.debugMode)
        {
            debugBall.GetComponent<MeshRenderer>().enabled = true;
        }
        else
        {
            debugBall.GetComponent<MeshRenderer>().enabled = false;
        }

        csvDict = CreateCSV();
    }

    // Update is called once per frame
    void Update()
    {
        //Time counter
        elapsedTime += Time.deltaTime;

        Vector3 down = shoulder.transform.TransformDirection(Vector3.down);
        Vector3 armVector = hand.transform.position - shoulder.transform.position;
        Vector3 targetVector = target.transform.position - hand.transform.position;
        Vector3 velocityVector = hand.GetComponent<VelocityEstimator>().GetVelocityEstimate();

        Vector2 targetVector2D = new Vector2(targetVector.x, targetVector.z);
        Vector2 handVelocity2D = new Vector2(velocityVector.x, velocityVector.z);

        angle = Vector3.SignedAngle(down, armVector, Vector3.left) * Mathf.Deg2Rad;
        absAngle = Mathf.Abs(angle);
        velocity = Mathf.Abs((absAngle - oldAngle) / Time.deltaTime);

        float angularAcceleration = Mathf.Abs((velocity - oldVelocity) / Time.deltaTime);

        oldAngle = absAngle;
        oldVelocity = velocity;

        //Lancia se sono soddisfatte le seguenti condizioni
        if (/*angularAcceleration * shoulderLength > (Physics.gravity * Mathf.Sin(angle)).magnitude &&*/ angle > adjustValueAngle && velocityVector.magnitude > 1.5 && Vector2.Dot(targetVector2D,handVelocity2D) > 0)
        {
            if(!StaticValues.debugMode && SE_setted)
            {
                SE_setted = false;
                ReleaseObject();
            }
            else if (StaticValues.debugMode)
            {
                ReleaseObject();
            }
            
        }
    }

    public void GrabBall()
    {
        if (!StaticValues.ballIsFlying && !StaticValues.ballIsGrabbed && !StaticValues.debugBallIsFlying && !StaticValues.debugBallIsGrabbed && StaticValues.enableGrab == true)
        {
            if(StaticValues.launchCounter == 0)
            {
                fbf_dataRecorder.StartRecordingData();
            }

            StaticValues.launchCounter++;
            uIManager.changeLaunchNumber();
            if (!StaticValues.debugMode)
            {
                supportRateManager.AdjustSupportRate();
            }
            ball.transform.position = hand.transform.position;
            collidingObject = ball;
            audioManager.PlayGrabSound();
            StaticValues.ballIsGrabbed = true;
            StaticValues.floorHit = false;
            StaticValues.debugBallIsGrabbed = true;
            StaticValues.debugFloorHit = false;
            if (!StaticValues.debugMode)
            {
                SE_indicator.GetComponent<Renderer>().material = M_SE_neutral;
            }
            

            objectInHand = collidingObject;
            collidingObject = null;
            FixedJoint joint = gameObject.AddComponent<FixedJoint>();
            joint.breakForce = 20000;
            joint.breakTorque = 20000;
            joint.connectedBody = objectInHand.GetComponent<Rigidbody>();
            if (StaticValues.moveTarget)
            {
                target.GetComponent<TargetManager>().MoveTarget();
            }
        }
    }
    public void SetSE(bool val)
    {
        if(StaticValues.ballIsGrabbed)
        {
            switch(val)
            {
                case true:
                    csvDict["Self efficacy"] = val.ToString();
                    SE_indicator.GetComponent<Renderer>().material = M_SE_positive;
                    break;
                case false:
                    csvDict["Self efficacy"] = val.ToString();
                    SE_indicator.GetComponent<Renderer>().material = M_SE_negative;
                    break;
            }

            SE_setted = true;
        }
    }

    private void ReleaseObject()
    {
        if (GetComponent<FixedJoint>())
        {
            //Remove the FixedJoint to release the ball
            GetComponent<FixedJoint>().connectedBody = null;
            Destroy(GetComponent<FixedJoint>());

            //Transfer position, velocity and angular velocity from hand to ball and debug ball

            Vector3 sourceVelocity = hand.GetComponent<VelocityEstimator>().GetVelocityEstimate();
            Vector3 sourceAngularVelocity = hand.GetComponent<VelocityEstimator>().GetAngularVelocityEstimate();
            Vector3 sourcePosition = hand.transform.position;

            ball.GetComponent<Rigidbody>().velocity = sourceVelocity;
            ball.GetComponent<Rigidbody>().angularVelocity = sourceAngularVelocity;
            ball.transform.position = sourcePosition;

            debugBall.GetComponent<Rigidbody>().velocity = sourceVelocity;
            debugBall.GetComponent<Rigidbody>().angularVelocity = sourceAngularVelocity;
            debugBall.transform.position = sourcePosition;

            //debugBall.GetComponent<DebugBallManager>().SetTargetPosition(target.transform.position);

            csvDict["Real velocity X"] = sourceVelocity.x.ToString();
            csvDict["Real velocity Y"] = sourceVelocity.y.ToString();
            csvDict["Real velocity Z"] = sourceVelocity.z.ToString();
            csvDict["Real velocity magnitude"] = sourceVelocity.magnitude.ToString();

            csvDict["Angular velocity X"] = sourceAngularVelocity.x.ToString();
            csvDict["Angular velocity Y"] = sourceAngularVelocity.y.ToString();
            csvDict["Angular velocity Z"] = sourceAngularVelocity.z.ToString();

            StaticValues.debugBallIsFlying = true;
            StaticValues.debugBallIsGrabbed = false;
            StaticValues.ballIsFlying = true;
            StaticValues.ballIsGrabbed = false;

            

            var tmpDict = enhancementManager.EnhanceLaunch(ball, target);


            csvDict["Elapsed time"] = elapsedTime.ToString();
            csvDict["Absolute time"] = DateTime.Now.Hour.ToString() + ":" +
                                            DateTime.Now.Minute.ToString() + ":" +
                                            DateTime.Now.Second.ToString() + ":" +
                                            DateTime.Now.Millisecond.ToString();

            csvDict["Target mode"] = StaticValues.targetChoice.ToString();
            csvDict["Support rate"] = StaticValues.supportRate.ToString();
            csvDict["Launch counter"] = StaticValues.launchCounter.ToString();
            csvDict["Enhancement mode"] = StaticValues.enhancementMode;


            csvDict["RedBall initial position X"] = ball.transform.position.x.ToString();
            csvDict["RedBall initial position Y"] = ball.transform.position.y.ToString();
            csvDict["RedBall initial position Z"] = ball.transform.position.z.ToString();

            csvDict["Target position X"] = target.transform.position.x.ToString();
            csvDict["Target position Y"] = target.transform.position.y.ToString();
            csvDict["Target position Z"] = target.transform.position.z.ToString();
            csvDict["Target rotation X"] = target.transform.rotation.eulerAngles.x.ToString();
            csvDict["Target rotation Y"] = target.transform.rotation.eulerAngles.y.ToString();
            csvDict["Target rotation Z"] = target.transform.rotation.eulerAngles.z.ToString();

            csvDict["Velocity force X"] = tmpDict["Velocity force X"];
            csvDict["Velocity force Y"] = tmpDict["Velocity force Y"];
            csvDict["Velocity force Z"] = tmpDict["Velocity force Z"];
            csvDict["Direction force X"] = tmpDict["Direction force X"];
            csvDict["Direction force Y"] = tmpDict["Direction force Y"];
            csvDict["Direction force Z"] = tmpDict["Direction force Z"];

            csvDict["Enhanced velocity X"] = tmpDict["Enhanced velocity X"];
            csvDict["Enhanced velocity Y"] = tmpDict["Enhanced velocity Y"];
            csvDict["Enhanced velocity Z"] = tmpDict["Enhanced velocity Z"];
            csvDict["Enhanced velocity magnitude"] = tmpDict["Enhanced velocity magnitude"];

            csvDict["Direction error"] = tmpDict["Direction error"];
            csvDict["Enhanced direction error"] = tmpDict["Enhanced direction error"];

            WritingCSVFile();
        }
        objectInHand = null;
    }
    /*private void TransferProperties(GameObject source, GameObject target)
    {
        Vector3 sourceVelocity = source.GetComponent<VelocityEstimator>().GetVelocityEstimate();
        Vector3 sourceAngularVelocity = source.GetComponent<VelocityEstimator>().GetAngularVelocityEstimate();
        Vector3 sourcePosition = source.transform.position;

        target.GetComponent<Rigidbody>().velocity = sourceVelocity;
        target.GetComponent<Rigidbody>().angularVelocity = sourceAngularVelocity;
        target.transform.position = sourcePosition;

        csvDict["Real velocity X"] = sourceVelocity.x.ToString();
        csvDict["Real velocity Y"] = sourceVelocity.y.ToString();
        csvDict["Real velocity Z"] = sourceVelocity.z.ToString();
        csvDict["Real velocity magnitude"] = sourceVelocity.magnitude.ToString();

        csvDict["Angular velocity X"] = sourceAngularVelocity.x.ToString();
        csvDict["Angular velocity Y"] = sourceAngularVelocity.y.ToString();
        csvDict["Angular velocity Z"] = sourceAngularVelocity.z.ToString();
    }*/

    private void OnTriggerEnter(Collider other)
    {
        if(collidingObject || !other.GetComponent<Rigidbody>()) { return; }
        collidingObject = other.gameObject;
    }

    private void OnTriggerExit(Collider other)
    {
        if(!collidingObject) { return; }
        collidingObject = null;
    }

    private Dictionary<string,string> CreateCSV()
    {
        Dictionary<string, string> dict = new()
        {
            { "Launch counter", "" },
            { "Elapsed time", "" },
            { "Absolute time", "" },
            { "Enhancement mode", "" },
            { "Target mode", "" },
            { "Support rate", "" },
            { "Self efficacy", "" },

            { "RedBall initial position X", "" },
            { "RedBall initial position Y", "" },
            { "RedBall initial position Z", "" },

            { "Target position X", "" },
            { "Target position Y", "" },
            { "Target position Z", "" },
            { "Target rotation X", "" },
            { "Target rotation Y", "" },
            { "Target rotation Z", "" },

            { "Real velocity X", "" },
            { "Real velocity Y", "" },
            { "Real velocity Z", "" },
            { "Real velocity magnitude", "" },

            { "Velocity force X", "" },
            { "Velocity force Y", "" },
            { "Velocity force Z", "" },
            { "Direction force X", "" },
            { "Direction force Y", "" },
            { "Direction force Z", "" },

            { "Enhanced velocity X", "" },
            { "Enhanced velocity Y", "" },
            { "Enhanced velocity Z", "" },
            { "Enhanced velocity magnitude", "" },

            { "Angular velocity X", "" },
            { "Angular velocity Y", "" },
            { "Angular velocity Z", "" },

            { "Direction error", "" },
            { "Enhanced direction error", "" },
        };

        string stringToWrite = "";
        foreach (string str in dict.Keys)
        {
            stringToWrite += (str + ",");
        }
        stringToWrite = stringToWrite.Remove(stringToWrite.Length - 1);

        _ = new FileInfo(CSVpath);
        TextWriter tw = new StreamWriter(CSVpath, true);
        tw.WriteLine(stringToWrite);
        tw.Close();

        return dict;
    }

    private void WritingCSVFile()
    {
        TextWriter tw = new StreamWriter(CSVpath, true);

        string stringToWrite = "";
        foreach (string value in csvDict.Values)
        {
            stringToWrite += (value + ",");
        }
        stringToWrite = stringToWrite.Remove(stringToWrite.Length - 1);

        tw.WriteLine(stringToWrite);
        tw.Close();
    }
}