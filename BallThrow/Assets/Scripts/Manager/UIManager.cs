using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private Text enhancementText, sliderText, SRNumber, launchNumber;
    [SerializeField]
    private Slider supportRateSlider;
    [SerializeField]
    private Canvas messageCanvas;

    void Start()
    {
        enhancementText.text = "Enhancement Mode: Support Rate";
        ChangeSliderText("SupportRate");
    }
    public void ChangeEnhancementText(string text)
    {
        enhancementText.text = "Enhancement Mode: " + text;
    }
    public void ChangeSliderText(string text)
    {
        switch (text)
        {
            case "OneTime":
                supportRateSlider.gameObject.SetActive(true);
                sliderText.text = "Direction Correction";
                break;
            case "SupportRate":
                supportRateSlider.gameObject.SetActive(true);
                sliderText.text = "Support Rate";
                break;
            default:
                supportRateSlider.gameObject.SetActive(false);
                break;
        }
    }
    public void UpdateSliderValue()
    {
        supportRateSlider.value = StaticValues.supportRate;
        SRNumber.text = StaticValues.supportRate.ToString() + "%";
    }
    public void EnableTrainingCanvas()
    {
        messageCanvas.GetComponentInChildren<Text>().text = "End of phase " + StaticValues.phaseNr;
        messageCanvas.gameObject.SetActive(true);
    }
    public void changeLaunchNumber()
    {
        launchNumber.text = "LAUNCH\n" + StaticValues.launchCounter;
    }
}
