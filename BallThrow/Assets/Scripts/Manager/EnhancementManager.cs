using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using UnityEngine;
using Valve.VR;

public class EnhancementManager : MonoBehaviour
{
    [SerializeField]
    UIManager uiManager;

    private Dictionary<string, string> enhancementDict;

    public enum EnhancementMode { None, OneTime, ContinuousForce, SupportRate }
    public EnhancementMode enhancementMode;

    void Start()
    {
        enhancementDict = new()
        {
            { "Velocity force X", "" },
            { "Velocity force Y", "" },
            { "Velocity force Z", "" },
            { "Direction force X", "" },
            { "Direction force Y", "" },
            { "Direction force Z", "" },

            { "Enhanced velocity X", "" },
            { "Enhanced velocity Y", "" },
            { "Enhanced velocity Z", "" },
            { "Enhanced velocity magnitude", "" },

            { "Direction error", "" },
            { "Enhanced direction error", "" },
        };

        enhancementMode = EnhancementMode.SupportRate;
        StaticValues.enhancementMode = enhancementMode.ToString();

    }

    private void ImprovingShotDirection(GameObject projectile, GameObject target)
    {
        Vector3 startVelocity = GetVelocity(projectile);
        Vector3 directionForce = Vector3.zero;
        float startVelocityMagnitude = startVelocity.magnitude;
        Vector2 targetDirection2D = new Vector2((target.transform.position - projectile.transform.position).x, (target.transform.position - projectile.transform.position).z).normalized;
        Vector2 launchDirection2D = new Vector2(startVelocity.normalized.x, startVelocity.normalized.z).normalized;
        if (enhancementMode == EnhancementMode.OneTime)
        {
            float angleOfError = Vector2.Angle(launchDirection2D, targetDirection2D);
            enhancementDict["Direction error"] = angleOfError.ToString();

            if (angleOfError < 90)
            {
                float coefficient = (float)StaticValues.supportRate / 100f;
                Vector2 improvedVelocity2D = Vector2.Lerp(launchDirection2D, targetDirection2D, coefficient).normalized;
                enhancementDict["Enhanced direction error"] = Vector2.Angle(improvedVelocity2D, targetDirection2D).ToString();
                Vector3 improvedVelocity3D = new Vector3(improvedVelocity2D.x, startVelocity.normalized.y, improvedVelocity2D.y).normalized * startVelocityMagnitude;
                SetVelocity(projectile, improvedVelocity3D);
            }
        }
        else if (enhancementMode == EnhancementMode.ContinuousForce)
        {
            float angleOfError = Vector2.SignedAngle(launchDirection2D, targetDirection2D);
            enhancementDict["Direction error"] = angleOfError.ToString();
            if (Mathf.Abs(angleOfError) < 75)
            {
                directionForce = projectile.GetComponent<ForceManager>().SetDirectionForce(angleOfError);
                enhancementDict["Direction force X"] = directionForce.x.ToString();
                enhancementDict["Direction force Y"] = directionForce.y.ToString();
                enhancementDict["Direction force Z"] = directionForce.z.ToString();

            }
        }
    }

    private void ImprovingShotVelocity(GameObject projectile, GameObject target)
    {
        Vector3 dropPoint = GetDropPoint(projectile, target);
        Vector3 startVelocity = GetVelocity(projectile);
        Vector3 velocityForce = Vector3.zero;
        float ballDistance = Vector3.Distance(projectile.transform.position, dropPoint);
        float targetDistance = Vector3.Distance(projectile.transform.position, target.transform.position);
        float error = targetDistance - ballDistance;

        if (enhancementMode == EnhancementMode.OneTime)
        {
            SetVelocity(projectile, startVelocity * (Mathf.Pow(1.2f, error)));
        }
        else if (enhancementMode == EnhancementMode.ContinuousForce)
        {
            velocityForce = projectile.GetComponent<ForceManager>().SetVelocityForce(error);
            enhancementDict["Velocity force X"] = velocityForce.x.ToString();
            enhancementDict["Velocity force Y"] = velocityForce.y.ToString();
            enhancementDict["Velocity force Z"] = velocityForce.z.ToString();
        }
        else if (enhancementMode == EnhancementMode.SupportRate)
        {
            Vector3 targetVelocity = GetTargetVelocity(projectile, target);
            Vector3 improvedVelocity = Vector3.Lerp(startVelocity, targetVelocity, (float)StaticValues.supportRate / 100f);
            SetVelocity(projectile, improvedVelocity);

        }

        //Update CSV dict
        enhancementDict["Enhanced velocity X"] = projectile.GetComponent<Rigidbody>().velocity.x.ToString();
        enhancementDict["Enhanced velocity Y"] = projectile.GetComponent<Rigidbody>().velocity.y.ToString();
        enhancementDict["Enhanced velocity Z"] = projectile.GetComponent<Rigidbody>().velocity.z.ToString();
        enhancementDict["Enhanced velocity magnitude"] = projectile.GetComponent<Rigidbody>().velocity.magnitude.ToString();
    }

    public void ChangeEnhancementMode()
    {
        switch (enhancementMode)
        {
            case EnhancementMode.SupportRate:
                enhancementMode = EnhancementMode.None;
                StaticValues.enhancementMode = enhancementMode.ToString();
                StaticValues.supportRate = 0;
                break;
            case EnhancementMode.None:
                enhancementMode = EnhancementMode.OneTime;
                StaticValues.enhancementMode = enhancementMode.ToString();
                StaticValues.supportRate = 0;
                break;
            case EnhancementMode.OneTime:
                enhancementMode = EnhancementMode.ContinuousForce;
                StaticValues.enhancementMode = enhancementMode.ToString();
                StaticValues.supportRate = 0;
                break;
            case EnhancementMode.ContinuousForce:
                enhancementMode = EnhancementMode.SupportRate;
                StaticValues.enhancementMode = enhancementMode.ToString();
                StaticValues.supportRate = 0;
                break;
        }

        uiManager.ChangeEnhancementText(StaticValues.enhancementMode.ToString());
        uiManager.ChangeSliderText(StaticValues.enhancementMode.ToString());
        uiManager.UpdateSliderValue();
    }

    public Dictionary <string,string> EnhanceLaunch(GameObject projectile, GameObject target)
    {
        ImprovingShotDirection(projectile, target);
        ImprovingShotVelocity(projectile, target);

        return enhancementDict;
    }

    private Vector3 GetVelocity(GameObject obj)
    {
        return obj.GetComponent<Rigidbody>().velocity;
    }

    private void SetVelocity(GameObject obj, Vector3 newVelocity)
    {
        obj.GetComponent<Rigidbody>().velocity = newVelocity;
    }

    private Vector3 GetDropPoint(GameObject projectile, GameObject target)
    {
        Vector3 projectileVelocity = GetVelocity(projectile);

        //Use of the projectile motion equation: we need the initial height:
        float initialHeight = projectile.transform.position.y - target.transform.position.y;

        // we need the time of flight and for this we resolve the 2nd grade equation, obtaining two different times ---> we use only the positive time
        float timeOfFlight = 0;
        float time1 = (projectileVelocity.y + Mathf.Sqrt(Mathf.Pow(-projectileVelocity.y, 2) + (2f * 9.81f * initialHeight))) / 9.81f;
        float time2 = (projectileVelocity.y - Mathf.Sqrt(Mathf.Pow(-projectileVelocity.y, 2) + (2f * 9.81f * initialHeight))) / 9.81f;

        if (Mathf.Sign(time1) > 0)
        {
            timeOfFlight = time1;
        }
        else if (Mathf.Sign(time2) > 0)
        {
            timeOfFlight = time2;
        }
        else
        {
            Debug.Log("time negative");
        }

        //the time of flight is calculated based on the component y of the initial velocity because it is the only component affected by the gravity force
        //after that, we use that time of flight to find the distance on the x and z components ---> uniform linear motion equation
        float xDistance = projectile.transform.position.x + (projectileVelocity.x * timeOfFlight);
        float zDistance = projectile.transform.position.z + (projectileVelocity.z * timeOfFlight);

        //now we have all the components of the point that the ball will hit in this launch (it is a prediction)
        Vector3 dropPoint = new(xDistance, target.transform.position.y, zDistance);

        return dropPoint;
    }

    private Vector3 GetTargetVelocity(GameObject projectile, GameObject target)
    {
        float initialHeight = projectile.transform.position.y - target.transform.position.y;
        float maxHeight = projectile.transform.position.y + 0.5f;
        float time1 = Mathf.Sqrt((2 * (maxHeight - initialHeight)) / 9.81f);
        float time2 = Mathf.Sqrt((2 * maxHeight) / 9.81f);
        float timeTot = time1 + time2;
        float yVelocity = 9.81f * time1;
        float xOffset = target.transform.position.x - projectile.transform.position.x;
        float zOffset = target.transform.position.z - projectile.transform.position.z;
        float xVelocity = xOffset / timeTot;
        float zVelocity = zOffset / timeTot;
        Vector3 targetVelocity = new(xVelocity, yVelocity, zVelocity);

        return targetVelocity;
    }

}
