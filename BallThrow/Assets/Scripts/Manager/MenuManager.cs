using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System.IO;

public class MenuManager : MonoBehaviour
{
    //This script manages the main menu
    //Var declarations
    [SerializeField]
    TMP_InputField inputName;
    [SerializeField]
    TMP_Dropdown dropdownTarget;
    [SerializeField]
    GameObject errorMessage;
    [SerializeField]
    TMP_Dropdown dropdownEye;
    [SerializeField]
    TMP_Dropdown dropdownHand;
    [SerializeField]
    TMP_Dropdown dropdownMoveTarget;
    [SerializeField]
    TMP_Dropdown dropdownPhase;

    //Method OnClick is called when START button is pressed
    //User must enter the name to continue, otherwise an error message will be displayed
    public void OnClickDebug()
    {
        errorMessage.SetActive(false);

        if (inputName.text != "")
        {
            StaticValues.userName = "DEBUG_" + inputName.text.Replace(" ", string.Empty);
            StaticValues.targetChoice = dropdownTarget.options[dropdownTarget.value].text;
            StaticValues.launchCounter = 0;
            StaticValues.eyeChoice = dropdownEye.options[dropdownEye.value].text;
            StaticValues.hand = dropdownHand.options[dropdownHand.value].text;
            if (dropdownTarget.options[dropdownTarget.value].text == "No")
            {
                StaticValues.moveTarget = false;
            }
            else
            {
                StaticValues.moveTarget = true;
            }
            StaticValues.phaseNr = int.Parse(dropdownPhase.options[dropdownPhase.value].text);
            StaticValues.debugMode = true;
            StaticValues.trainingMode = false;
            CreateCSVFolder();
            SceneManager.LoadScene(2);
        }
        else
        {
            errorMessage.SetActive(true);
        }
    }
    public void OnClickTraining()
    {
        errorMessage.SetActive(false );

        if (inputName.text != "")
        {
            StaticValues.userName = "TRAINING_" + inputName.text.Replace(" ", string.Empty);
            StaticValues.targetChoice = dropdownTarget.options[dropdownTarget.value].text;
            StaticValues.launchCounter = 0;
            StaticValues.eyeChoice = dropdownEye.options[dropdownEye.value].text;
            StaticValues.hand = dropdownHand.options[dropdownHand.value].text;
            StaticValues.debugMode = false;
            StaticValues.trainingMode = true;
            if (dropdownTarget.options[dropdownTarget.value].text == "No")
            {
                StaticValues.moveTarget = false;
            }
            else
            {
                StaticValues.moveTarget = true;
            }
            StaticValues.phaseNr = int.Parse(dropdownPhase.options[dropdownPhase.value].text);
            CreateCSVFolder();
            SceneManager.LoadScene(1);
        }
        else
        {
            errorMessage.SetActive(true);
        }
    }
    public void OnClickStart()
    {
        errorMessage.SetActive(false);

        if (inputName.text != "")
        {
            StaticValues.userName = inputName.text.Replace(" ", string.Empty);
            StaticValues.targetChoice = dropdownTarget.options[dropdownTarget.value].text;
            StaticValues.launchCounter = 0;
            StaticValues.eyeChoice = dropdownEye.options[dropdownEye.value].text;
            StaticValues.hand = dropdownHand.options[dropdownHand.value].text;
            StaticValues.debugMode = false;
            StaticValues.trainingMode = false;
            if (dropdownMoveTarget.options[dropdownMoveTarget.value].text == "No")
            {
                StaticValues.moveTarget = false;
            }
            else
            {
                StaticValues.moveTarget = true;
            }
            StaticValues.phaseNr = int.Parse(dropdownPhase.options[dropdownPhase.value].text);
            CreateCSVFolder();
            SceneManager.LoadScene(1);
        }
        else
        {
            errorMessage.SetActive(true);
        }
    }
    
    private void CreateCSVFolder()
    {
        StaticValues.pathGeneral = "/CSV Data/" + StaticValues.userName + "/" + StaticValues.phaseNr + "/GENERAL";
        StaticValues.pathLaunchByLaunch = "/CSV Data/" + StaticValues.userName + "/" + StaticValues.phaseNr + "/FRAMEBYFRAME";
        Directory.CreateDirectory(Application.dataPath + StaticValues.pathGeneral);
        Directory.CreateDirectory(Application.dataPath + StaticValues.pathLaunchByLaunch);
    }
}
