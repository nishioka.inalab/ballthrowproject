using UnityEngine;
using UnityEngine;
using Valve.VR;
using UnityEngine.UI;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;

public class ControlManager : MonoBehaviour
{
    [SerializeField]
    private GameObject hand;
    [SerializeField]
    private EnhancementManager enhancementManager;
    [SerializeField]
    private ThrowManager throwManager;
    [SerializeField]
    private UIManager uiManager;
    [SerializeField]
    private Slider slider;

    private bool visualizeBlueBall = false;

    private readonly SteamVR_Action_Boolean grabBall = SteamVR_Input.GetBooleanAction("GrabBall");
    private readonly SteamVR_Action_Boolean setShoulder = SteamVR_Actions._default.SetShoulder;
    private readonly SteamVR_Action_Boolean increaseSupportRate = SteamVR_Actions._default.IncreaseSupportRate;
    private readonly SteamVR_Action_Boolean decreaseSupportRate = SteamVR_Actions._default.DecreaseSupportRate;
    private readonly SteamVR_Action_Boolean changeEnhancementMode = SteamVR_Actions._default.ChangeEnhancementMode;
    private readonly SteamVR_Action_Boolean setDebugMode = SteamVR_Actions._default.SetDebugMode;
    private readonly SteamVR_Action_Boolean selfEfficacyUp = SteamVR_Actions._default.SelfEfficacyUp;
    private readonly SteamVR_Action_Boolean selfEfficacyDown = SteamVR_Actions._default.SelfEfficacyDown;

    void Start()
    {
        grabBall.AddOnStateDownListener(GrabBall, SteamVR_Input_Sources.Any);
        setShoulder.AddOnStateDownListener(SetShoulder, SteamVR_Input_Sources.Any);
        if (StaticValues.debugMode)
        {
            increaseSupportRate.AddOnStateDownListener(IncreaseSupportRate, SteamVR_Input_Sources.Any);
            decreaseSupportRate.AddOnStateDownListener(DecreaseSupportRate, SteamVR_Input_Sources.Any);
            changeEnhancementMode.AddOnStateDownListener(ChangeEnhancementMode, SteamVR_Input_Sources.Any);
            setDebugMode.AddOnStateDownListener(VisualizeBlueBall, SteamVR_Input_Sources.Any);
        }
        else if (!StaticValues.debugMode)
        {
            slider.onValueChanged.AddListener(delegate { SupportRateUpdate(); });
            selfEfficacyUp.AddOnStateDownListener(SelfEfficacyUp, SteamVR_Input_Sources.Any);
            selfEfficacyDown.AddOnStateDownListener(SelfEfficacyDown, SteamVR_Input_Sources.Any);
        }
    }

    private void Update()
    {
        if (!StaticValues.debugMode)
        {
            CheckKeyPressed();
        }
    }

    public void IncreaseSupportRate(SteamVR_Action fromAction, SteamVR_Input_Sources fromSource)
    {
        if (StaticValues.supportRate < 100)
        {
            StaticValues.supportRate += 1;
            uiManager.UpdateSliderValue();
        }
    }

    public void DecreaseSupportRate(SteamVR_Action fromAction, SteamVR_Input_Sources fromSource)
    {
        if (StaticValues.supportRate > 0)
        {
            StaticValues.supportRate -= 1;
            uiManager.UpdateSliderValue();
        }
    }

    public void SetShoulder(SteamVR_Action fromAction, SteamVR_Input_Sources fromSource)
    {
    }

    public void GrabBall(SteamVR_Action fromAction, SteamVR_Input_Sources fromSource)
    {
        throwManager.GrabBall();
    }

    public void ChangeEnhancementMode(SteamVR_Action fromAction, SteamVR_Input_Sources fromSource)
    {
        enhancementManager.ChangeEnhancementMode();
    }

    public void VisualizeBlueBall(SteamVR_Action fromAction, SteamVR_Input_Sources fromSource)
    {
        visualizeBlueBall = !visualizeBlueBall;    
    }
    
    public void SelfEfficacyUp(SteamVR_Action fromAction, SteamVR_Input_Sources fromSource)
    {
        throwManager.SetSE(true);
    }
    public void SelfEfficacyDown(SteamVR_Action fromAction, SteamVR_Input_Sources fromSource)
    {
        throwManager.SetSE(false);
    }
    private void CheckKeyPressed()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            IncreaseSupportRate(increaseSupportRate, SteamVR_Input_Sources.Any);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            DecreaseSupportRate(decreaseSupportRate, SteamVR_Input_Sources.Any);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            ChangeEnhancementMode(changeEnhancementMode, SteamVR_Input_Sources.Any);
        }
    }
    public void SupportRateUpdate()
    {
        StaticValues.supportRate = ((int)slider.value);
        uiManager.UpdateSliderValue();
    }
}
