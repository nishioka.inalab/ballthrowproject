using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class PreliminaryExperimentManager : MonoBehaviour
{
    private int previousLaunch = 0;
    private static readonly string dataSeparator = ",";
    private readonly List<string> dataList = new();
    private string[] dataHeaders;
    private string fileName;
    [SerializeField]
    private Button trueButton, falseButton;
    // Start is called before the first frame update
    void Start()
    {
        if(StaticValues.phaseNr == 3)
        {
            fileName = "/CHANGE_CHECK_";
            dataHeaders = new string[]
            {
                "Launch number",
                "Change noticed"
            };
            trueButton.gameObject.SetActive(true);
            falseButton.gameObject.SetActive(true);
        }
        else if(StaticValues.phaseNr == 4)
        {
            fileName = "/SATISFACTION_CHECK_";
            dataHeaders = new string[]
            {
                "Launch number",
                "User satisfied"
            };
            trueButton.gameObject.SetActive(true);
            falseButton.gameObject.SetActive(true);
        }
    }

    public void CompileCSV(bool val)
    {
        if(!StaticValues.ballIsFlying && !StaticValues.ballIsGrabbed && StaticValues.launchCounter == (previousLaunch + 1))
        {
            previousLaunch++;
            dataList.Add(StaticValues.launchCounter.ToString());
            if (StaticValues.phaseNr == 3 && StaticValues.launchCounter % 10 == 1)
            {
                dataList.Add("NV");
            }
            else
            {
                dataList.Add(val.ToString());
            }
            AppendData(dataList.ToArray());
            dataList.Clear();
            if(StaticValues.launchCounter < 50)
            {
                StaticValues.enableGrab = true;
            }
            
        }
    }

    private void VerifyDirectory()
    {
        string dir = GetDirectoryPath();
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
    }

    private void VerifyFile()
    {
        string file = GetFilePath();
        if (!File.Exists(file))
        {
            CreateDataFile();
        }
    }

    private string GetDirectoryPath()
    {
        return Application.dataPath + StaticValues.pathGeneral;
    }

    private string GetFilePath()
    {
        return GetDirectoryPath() + fileName + StaticValues.userName + "_" + StaticValues.phaseNr + ".csv";
    }

    public void AppendData(string[] dataString)
    {
        VerifyDirectory();
        VerifyFile();

        TextWriter tw = new StreamWriter(GetFilePath(), true);

        string stringToWrite = "";
        foreach (string str in dataString)
        {
            if (stringToWrite != "")
            {
                stringToWrite += dataSeparator;
            }
            stringToWrite += str;
        }

        tw.WriteLine(stringToWrite);
        tw.Close();
    }

    public void CreateDataFile()
    {
        VerifyDirectory();
        string stringToWrite = "";
        foreach (string str in dataHeaders)
        {
            if (stringToWrite != "")
            {
                stringToWrite += dataSeparator;
            }
            stringToWrite += str;
        }
        _ = new FileInfo(GetFilePath());
        TextWriter tw = new StreamWriter(GetFilePath(), true);
        tw.WriteLine(stringToWrite);
        tw.Close();
    }






}
