using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceManager : MonoBehaviour
{
    private bool flyingBall = false, velocityModification = false, directionModification = false;
    private float currentVelocityError = 0, currentDirectionError = 0;
    private Vector3 velocityForce, directionForce;

    private void FixedUpdate()
    {
        if(flyingBall)
        {
            if(velocityModification)
            {
                this.GetComponent<Rigidbody>().AddForce(velocityForce, ForceMode.Force);
            }

            if(directionModification) 
            {
                this.GetComponent<Rigidbody>().AddForce(directionForce, ForceMode.Force);
            }
            
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        flyingBall = false;
        velocityModification= false;
        directionModification= false;
    }

    public Vector3 SetVelocityForce(float error)
    {
        flyingBall = true;
        velocityModification= true;        
        currentVelocityError= error;
        if (currentVelocityError > 0)
        {
            velocityForce = this.GetComponent<Rigidbody>().velocity.normalized * (Mathf.Pow(1.5f, Mathf.Abs(currentVelocityError) - 1));
        }
        else
        {
            velocityForce = - this.GetComponent<Rigidbody>().velocity.normalized * (Mathf.Pow(1.5f, Mathf.Abs(currentVelocityError) - 1));
        }

        return velocityForce;
    }

    public Vector3 SetDirectionForce(float error)
    {
        flyingBall = true;
        directionModification = true;
        currentDirectionError= error;
        if (currentDirectionError > 0)
        {
            directionForce = Vector3.left * (Mathf.Pow(1.5f, Mathf.Abs(currentDirectionError) / 15) - 1);
        }
        else
        {
            directionForce = Vector3.right * (Mathf.Pow(1.5f, Mathf.Abs(currentDirectionError) / 15) - 1);
        }

        return directionForce;
    }


}
