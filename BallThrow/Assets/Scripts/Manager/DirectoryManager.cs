using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DirectoryManager : MonoBehaviour
{
    private string dir1 = "/CSV Data/" + UserValues.userName + "/GENERAL";
    private string dir2 = "/CSV Data/" + UserValues.userName + "/FRAMEBYFRAME";
    private void Awake()
    {
        Directory.CreateDirectory(Application.dataPath + dir1);
        Directory.CreateDirectory(Application.dataPath + dir2);
        UserValues.pathGeneral = dir1;
        UserValues.pathLaunchByLaunch = dir2;
    }
}
