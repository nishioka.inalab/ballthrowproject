using UnityEngine;

public class TargetManager : MonoBehaviour
{
    [SerializeField]
    private GameObject wall, breakEffect;
    private bool lastHit = false;

    private void Start()
    {
        if (StaticValues.targetChoice == "Air")
        {
            wall.SetActive(true);
            this.transform.position = new Vector3(0, 0.5f, 3.5f);
            this.transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else
        {
            this.transform.position = new Vector3(0, 0, 4.5f);
            this.transform.eulerAngles = new Vector3(270, 180, 0);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Ball" && StaticValues.floorHit == false)
        {
            GameObject effect = Instantiate(breakEffect) as GameObject;
            effect.transform.position = gameObject.transform.position;
            if (StaticValues.moveTarget)
            {
                this.GetComponent<MeshRenderer>().enabled = false;
                lastHit = true;
            }
            
        }
    }

    public void MoveTarget()
    {
        if (lastHit)
        {
            this.GetComponent<MeshRenderer>().enabled = true;
            float positionX = Random.Range(-2f, 2f);
            float positionZ = Random.Range(3.5f, 6f);
            Vector3 targetNewPosition = new(positionX, 0, positionZ);
            this.transform.position = targetNewPosition;
            this.GetComponent<MeshRenderer>().enabled = true;
            lastHit = false;
        }
        
    }
}
