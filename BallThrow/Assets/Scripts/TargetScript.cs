using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetScript : MonoBehaviour
{
    [SerializeField]
    private GameObject wall;
    void Start()
    {
        if (StaticValues.targetChoice == "Air")
        {
            wall.SetActive(true);
            this.transform.position = new Vector3(0, 0.5f, 3.5f);
            this.transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else
        {
            this.transform.position = new Vector3(0, 0, 3.5f);
            this.transform.eulerAngles = new Vector3(270, 180, 0);
        }
    }
}
