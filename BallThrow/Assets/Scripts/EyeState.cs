using System;
using UnityEngine;
using ViveSR.anipal.Eye;

public class EyeState : MonoBehaviour
{
    [HideInInspector]
    public float leftOpenness, rightOpenness, leftPupilDiameter, rightPupilDiameter;
    [HideInInspector]
    public Vector2 leftPupilPosition, rightPupilPosition;
    [HideInInspector]
    public bool blinkFlag;
    [HideInInspector]
    public string focusObject;

    void Update()
    {
        SRanipal_Eye.GetVerboseData(out VerboseData verboseData);
        SRanipal_Eye.Focus(GazeIndex.COMBINE, out Ray ray, out FocusInfo focusInfo);
        try
        {
            focusObject = focusInfo.collider.gameObject.name;
        }
        catch(NullReferenceException e)
        {
            focusObject = "None";
        }
     
        leftOpenness = verboseData.left.eye_openness;
        rightOpenness = verboseData.right.eye_openness;
        leftPupilDiameter = verboseData.left.pupil_diameter_mm;
        rightPupilDiameter = verboseData.right.pupil_diameter_mm;
        leftPupilPosition = verboseData.left.pupil_position_in_sensor_area;
        rightPupilPosition = verboseData.right.pupil_position_in_sensor_area;

        if(leftOpenness < 0.2f && rightOpenness < 0.2f)
        {
            blinkFlag = true;
        }
        else
        {
            blinkFlag = false;
        }
    }
}
