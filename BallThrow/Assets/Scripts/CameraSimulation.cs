using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class CameraSimulation : MonoBehaviour
{
    [SerializeField] private LineRenderer GazeRayRenderer;
    private static string dataDirectoryName = "CSV Simulation";
    private static string dataFileName = "b.csv";
    private static string dataSeparator = ",";
    Dictionary<string, float> dataDict = new Dictionary<string, float>();
    public int LengthOfRay = 25;
    [SerializeField]
    // Start is called before the first frame update
    void Start()
    {
        System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
        customCulture.NumberFormat.NumberDecimalSeparator = ".";

        System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

        StartCoroutine(ReadCSV());
    }

    IEnumerator ReadCSV()
    {
        float prevTime = 0;
        int rowCounter = 0;
        string[] headWords = null;
        StreamReader tr = new StreamReader(GetFilePath());
        while (!tr.EndOfStream)
        {
            rowCounter++;
            string currentRow = tr.ReadLine();

            if (rowCounter == 1)
            {
                headWords = currentRow.Split(dataSeparator);
                foreach (var word in headWords)
                {
                    dataDict.Add(word, 0);
                }
            }
            else
            {
                string[] values = currentRow.Split(dataSeparator);

                for (int i = 0; i < headWords.Length; i++)
                {
                    dataDict[headWords[i]] = float.Parse(values[i]);
                }

                yield return new WaitForSeconds(dataDict["Elapsed time"] - prevTime);
                prevTime = dataDict["Elapsed time"];
                Simulation();
            }


        }
        tr.Close();
        yield return null;
    }

    private void Simulation()
    {

        Vector3 cameraPosition = new Vector3(dataDict["HMD position X"], dataDict["HMD position Y"], dataDict["HMD position Z"]);
        Vector3 cameraRotation = new Vector3(dataDict["HMD rotation X"], dataDict["HMD rotation Y"], dataDict["HMD rotation Z"]);
        Vector3 GazeVector = new Vector3(dataDict["Gaze X"], dataDict["Gaze Y"], dataDict["Gaze Z"]);

        transform.position = cameraPosition;
        transform.eulerAngles= cameraRotation;
        GazeRayRenderer.SetPosition(0, transform.position - transform.up * 0.05f);
        GazeRayRenderer.SetPosition(1, transform.position + GazeVector * LengthOfRay);
    }


    static string GetDirectoryPath()
    {
        return Application.dataPath + "/" + dataDirectoryName;
    }

    static string GetFilePath()
    {
        return GetDirectoryPath() + "/" + dataFileName;
    }
}
