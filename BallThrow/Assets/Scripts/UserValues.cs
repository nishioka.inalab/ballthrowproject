using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserValues 
{

    public static string userName { get; set; }
    public static string targetChoice { get; set; }
    public static string eyeChoice { get; set; }
    public static int launchCounter { get; set; }
    public static string pathGeneral { get; set; }
    public static string pathLaunchByLaunch { get; set; }
    public static int supportRate { get; set; }
    public static string enhancementMode { get; set; }
    public static string adjustMode { get; set; }
    public static bool ballIsFlying { get; set; }
    public static bool ballIsGrabbed { get; set; }
}
