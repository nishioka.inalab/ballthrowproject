using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;//per le misure di accelerazione
using UnityEngine.UI;//per UI
using System.IO;
using Valve.VR;
using UnityEngine.SceneManagement;

public class BallHandThrow1 : MonoBehaviour
{
    //------------------------------------------------------------------- Variables declaration -------------------------------------------------------------------
    //Enhancement modality
    public enum Enhancement {None, OneTime, ContinuousForce, SupportRate}
    public Enhancement enhancement;
    private int enhancementModeCounter = 0;

    //Debug mode
    private bool debugMode = false;

    //GameObjects
    [SerializeField]
    private GameObject hand, ball, debugBall, target, shoulder;
    private GameObject objectInHand;
    private GameObject collidingObject;

    //Numerical var
    private int launchNr = 0;
    private float angle, oldAngle = 0f;
    private float velocity, oldVelocity = 0f;
    private float elapsedTime = 0f;

    //Readonly values
    private readonly float adjustValueAngle = 0.5f;
    private readonly float shoulderLength = 0.5f;

    //UI components
    [SerializeField]
    private Slider supportRateSlider;
    [SerializeField]
    private Text enhancementText, debugText, sliderText;
    [SerializeField]
    private GameObject canvas;
    [SerializeField]
    FrameByFrame_DataRecorder fbf_dataRecorder;

    private readonly SteamVR_Action_Boolean grabBall = SteamVR_Input.GetBooleanAction("GrabBall");
    private readonly SteamVR_Action_Boolean setShoulder = SteamVR_Actions._default.SetShoulder;
    private readonly SteamVR_Action_Boolean increaseSupportRate = SteamVR_Actions._default.IncreaseSupportRate;
    private readonly SteamVR_Action_Boolean decreaseSupportRate = SteamVR_Actions._default.DecreaseSupportRate;
    private readonly SteamVR_Action_Boolean changeEnhancementMode = SteamVR_Actions._default.ChangeEnhancementMode;
    private readonly SteamVR_Action_Boolean setDebugMode = SteamVR_Actions._default.SetDebugMode;

    //Data dictionary
    private Dictionary<string, string> csvDict;

    //CSV path
    private string CSVpath = "";

    //-------------------------------------------------------------------------------------------------------------------------------------------------------------

    //----------------------------------------------------------------- Start and Update functions ----------------------------------------------------------------
    // Start is called before the first frame update
    void Start()
    {
        CSVpath = Application.dataPath + UserValues.pathGeneral + "/GENUERAL_" + UserValues.userName + "_DATE_" + System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".csv";

        //Set the number decimal separator to avoid problems with the CSV file format
        System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
        customCulture.NumberFormat.NumberDecimalSeparator = ".";
        System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

        //Set enhancement mode to "None" and debug mode to "false"
        enhancement = Enhancement.None;
        UserValues.enhancementMode = enhancement.ToString();

        UserValues.ballIsFlying = false;
        UserValues.ballIsGrabbed = false;


        //Add listeners for HTC Vive controller
        grabBall.AddOnStateDownListener(GrabBall, SteamVR_Input_Sources.Any);
        setShoulder.AddOnStateDownListener(SetShoulder, SteamVR_Input_Sources.Any);
        if(SceneManager.GetActiveScene().name == "SampleScene")
        {
            increaseSupportRate.AddOnStateDownListener(IncreaseSupportRate, SteamVR_Input_Sources.Any);
            decreaseSupportRate.AddOnStateDownListener(DecreaseSupportRate, SteamVR_Input_Sources.Any);
            changeEnhancementMode.AddOnStateDownListener(ChangeEnhancementMode, SteamVR_Input_Sources.Any);
            setDebugMode.AddOnStateDownListener(SetDebugMode, SteamVR_Input_Sources.Any);
        }
        else if(SceneManager.GetActiveScene().name == "ExperimentScene")
        {
            supportRateSlider.onValueChanged.AddListener(delegate { SupportRateUpdate(); });
        }
        

        //Set the canvas active
        canvas.SetActive(true);
        supportRateSlider.gameObject.SetActive(false);

        //Create the CSV file and the dictionary containing all the simulation informations
        csvDict = CreateCSV();
        UserValues.launchCounter = launchNr;
    }

    // Update is called once per frame
    void Update()
    {
        if(SceneManager.GetActiveScene().name == "ExperimentScene")
        {
            CheckKeyPressed();
        }

        //Time counter
        elapsedTime += Time.deltaTime;

        Vector3 down = shoulder.transform.TransformDirection(Vector3.down);
        Vector3 targetDirection = hand.transform.position - shoulder.transform.position;

        angle = Vector3.Angle(down,targetDirection) * Mathf.Deg2Rad;
        velocity = Mathf.Abs((angle - oldAngle) / Time.deltaTime);

        float angularAcceleration = Mathf.Abs((velocity - oldVelocity) / Time.deltaTime);

        oldAngle = angle;
        oldVelocity = velocity;

        //Lancia se sono soddisfatte le seguenti condizioni
        if (angularAcceleration * shoulderLength > (Physics.gravity * Mathf.Sin(angle)).magnitude && angle > adjustValueAngle)
        {
            ReleaseObject();
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------

    //---------------------------------------------------------------- Launch enhancement functions ----------------------------------------------------------------
    //Direction enhancement
    private void ImprovingShotDirection(GameObject projectile, GameObject target)
    {
        Vector3 startVelocity = GetVelocity(projectile);
        Vector3 directionForce = Vector3.zero;
        float startVelocityMagnitude = startVelocity.magnitude;   
        Vector2 targetDirection2D = new Vector2((target.transform.position - projectile.transform.position).x, (target.transform.position - projectile.transform.position).z).normalized;
        Vector2 launchDirection2D = new Vector2(startVelocity.normalized.x, startVelocity.normalized.z).normalized;
        if(enhancement == Enhancement.OneTime)
        {
            float angleOfError = Vector2.Angle(launchDirection2D, targetDirection2D);
            csvDict["Original angle of error"] = angleOfError.ToString();

            if (angleOfError < 90)
            {
                float coefficient = (float)UserValues.supportRate / 100f;
                Vector2 improvedVelocity2D = Vector2.Lerp(launchDirection2D, targetDirection2D, coefficient).normalized;
                csvDict["Amount of angle improvement"] = Vector2.Angle(launchDirection2D, improvedVelocity2D).ToString();
                csvDict["Modified angle of error"] = Vector2.Angle(improvedVelocity2D, targetDirection2D).ToString();
                Vector3 improvedVelocity3D = new Vector3(improvedVelocity2D.x, startVelocity.normalized.y, improvedVelocity2D.y).normalized * startVelocityMagnitude;
                SetVelocity(projectile, improvedVelocity3D);
            }
        }
        else if (enhancement == Enhancement.ContinuousForce)
        {
            float angleOfError = Vector2.SignedAngle(launchDirection2D, targetDirection2D);
            csvDict["Original angle of error"] = angleOfError.ToString();
            if(Mathf.Abs(angleOfError) < 75)
            {
                directionForce = projectile.GetComponent<ForceManager>().SetDirectionForce(angleOfError);
                csvDict["Direction force X"] = directionForce.x.ToString();
                csvDict["Direction force Y"] = directionForce.y.ToString();
                csvDict["Direction force Z"] = directionForce.z.ToString();
            }
        }
    }

    //Velocity enhancement
    private void ImprovingShotVelocity(GameObject projectile, GameObject target)
    {
        Vector3 dropPoint = GetDropPoint(projectile, target);
        Vector3 startVelocity = GetVelocity(projectile);
        Vector3 velocityForce = Vector3.zero;
        float ballDistance = Vector3.Distance(projectile.transform.position, dropPoint);
        float targetDistance = Vector3.Distance(projectile.transform.position, target.transform.position);
        float error = targetDistance - ballDistance;

        if(enhancement == Enhancement.OneTime)
        {
            SetVelocity(projectile, startVelocity * (Mathf.Pow(1.2f,error)));
        }
        else if(enhancement == Enhancement.ContinuousForce)
        {
            velocityForce = projectile.GetComponent<ForceManager>().SetVelocityForce(error);
            csvDict["Velocity force X"] = velocityForce.x.ToString();
            csvDict["Velocity force Y"] = velocityForce.y.ToString();
            csvDict["Velocity force Z"] = velocityForce.z.ToString();
        }
        else if (enhancement == Enhancement.SupportRate)
        {
            Vector3 targetVelocity = GetTargetVelocity(projectile, target);
            Vector3 improvedVelocity = Vector3.Lerp(startVelocity, targetVelocity, (float)UserValues.supportRate / 100f);
            SetVelocity(projectile,improvedVelocity);

        }

        //Update CSV dict
        csvDict["Modified velocity X"] = projectile.GetComponent<Rigidbody>().velocity.x.ToString();
        csvDict["Modified velocity Y"] = projectile.GetComponent<Rigidbody>().velocity.y.ToString();
        csvDict["Modified velocity Z"] = projectile.GetComponent<Rigidbody>().velocity.z.ToString();
        csvDict["Modified velocity magnitude"] = projectile.GetComponent<Rigidbody>().velocity.magnitude.ToString();
     }
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------

    //------------------------------------------------------------------ Ball properties functions -----------------------------------------------------------------
    //Get the velocity of the object obj
    private Vector3 GetVelocity(GameObject obj)
    {
        return obj.GetComponent<Rigidbody>().velocity;
    }
    
    //Set the velocity of the object obj
    private void SetVelocity(GameObject obj, Vector3 newVelocity)
    {
        obj.GetComponent<Rigidbody>().velocity = newVelocity;
    }

    //Calcolate the projectile position at the same height of the target
    private Vector3 GetDropPoint(GameObject projectile, GameObject target)
    {
        Vector3 projectileVelocity = GetVelocity(projectile);

        //Use of the projectile motion equation: we need the initial height:
        float initialHeight = projectile.transform.position.y - target.transform.position.y;

        // we need the time of flight and for this we resolve the 2nd grade equation, obtaining two different times ---> we use only the positive time
        float timeOfFlight = 0;
        float time1 = (projectileVelocity.y + Mathf.Sqrt(Mathf.Pow(-projectileVelocity.y, 2) + (2f * 9.81f * initialHeight))) / 9.81f;
        float time2 = (projectileVelocity.y - Mathf.Sqrt(Mathf.Pow(-projectileVelocity.y, 2) + (2f * 9.81f * initialHeight))) / 9.81f;

        if (Mathf.Sign(time1) > 0)
        {
            timeOfFlight = time1;
        }
        else if (Mathf.Sign(time2) > 0)
        {
            timeOfFlight = time2;
        }
        else
        {
            Debug.Log("time negative");
        }

        //the time of flight is calculated based on the component y of the initial velocity because it is the only component affected by the gravity force
        //after that, we use that time of flight to find the distance on the x and z components ---> uniform linear motion equation
        float xDistance = projectile.transform.position.x + (projectileVelocity.x * timeOfFlight);
        float zDistance = projectile.transform.position.z + (projectileVelocity.z * timeOfFlight);

        //now we have all the components of the point that the ball will hit in this launch (it is a prediction)
        Vector3 dropPoint = new(xDistance, target.transform.position.y, zDistance);

        return dropPoint;
    }

    //Get the exact needed velocity to hit the target perfectly
    private Vector3 GetTargetVelocity(GameObject projectile, GameObject target)
    {
        float initialHeight = projectile.transform.position.y - target.transform.position.y;
        float maxHeight = projectile.transform.position.y + 0.5f;
        float time1 = Mathf.Sqrt((2 * (maxHeight - initialHeight))/9.81f);
        float time2 = Mathf.Sqrt((2 * maxHeight) / 9.81f);
        float timeTot = time1 + time2;
        float yVelocity = 9.81f * time1;
        float xOffset = target.transform.position.x - projectile.transform.position.x;
        float zOffset = target.transform.position.z - projectile.transform.position.z;
        float xVelocity = xOffset / timeTot;
        float zVelocity = zOffset / timeTot;
        Vector3 targetVelocity = new(xVelocity, yVelocity, zVelocity);

        return targetVelocity;
    }
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------- HTC Vive controller functions ---------------------------------------------------------------
    //Increase the support rate by pressing nord touchpad
    public void IncreaseSupportRate(SteamVR_Action fromAction, SteamVR_Input_Sources fromSource)
    {
        if (UserValues.supportRate < 100)
        {
            UserValues.supportRate += 1;
            supportRateSlider.value = UserValues.supportRate;
        }
    }

    //Decrease the support rate by pressing sud touchpad
    public void DecreaseSupportRate(SteamVR_Action fromAction, SteamVR_Input_Sources fromSource)
    {
        if (UserValues.supportRate > 0)
        {
            UserValues.supportRate -= 1;
            supportRateSlider.value = UserValues.supportRate;
        }
    }

    //Set the position of the shoulder by pressing lateral button
    public void SetShoulder(SteamVR_Action fromAction, SteamVR_Input_Sources fromSource)
    {
        shoulder.transform.position = new Vector3(hand.transform.position.x, hand.transform.position.y + shoulderLength, hand.transform.position.z);
    }

    //Grab the ball pressing grip button
    public void GrabBall(SteamVR_Action fromAction, SteamVR_Input_Sources fromSource)
    {
        if(!UserValues.ballIsFlying && !UserValues.ballIsGrabbed)
        {
            if (launchNr == 0)
            {
                fbf_dataRecorder.StartRecordingData();
            }
            if (debugMode)
            {
                debugBall.GetComponent<MeshRenderer>().enabled = false;
            }
            launchNr++;
            UserValues.launchCounter++;
            ball.transform.position = hand.transform.position;
            collidingObject = ball;
            UserValues.ballIsGrabbed = true;
            GrabObject();
        }
    }

    //Change enhancement mode
    public void ChangeEnhancementMode(SteamVR_Action fromAction, SteamVR_Input_Sources fromSource)
    {
        enhancementModeCounter = (enhancementModeCounter + 1) % 4;
        switch (enhancementModeCounter)
        {
            case 0: enhancement = Enhancement.None;
                    UserValues.enhancementMode = enhancement.ToString();
                    enhancementText.text = "Enhancement Mode: None";
                    UserValues.supportRate = 0;
                    supportRateSlider.value = UserValues.supportRate;
                    supportRateSlider.gameObject.SetActive(false);
                    break;
            case 1: enhancement = Enhancement.OneTime;
                    UserValues.enhancementMode = enhancement.ToString();
                    enhancementText.text = "Enhancement Mode: OneTime";
                    sliderText.text = "Direction Correction:";
                    UserValues.supportRate = 0;
                    supportRateSlider.value = UserValues.supportRate;
                    supportRateSlider.gameObject.SetActive(true);
                    break;
            case 2: enhancement = Enhancement.ContinuousForce;
                    UserValues.enhancementMode = enhancement.ToString();
                    enhancementText.text = "Enhancement Mode: ContinuousForce";
                    UserValues.supportRate = 0;
                    supportRateSlider.value = UserValues.supportRate;
                    supportRateSlider.gameObject.SetActive(false);
                    break;
            case 3: enhancement = Enhancement.SupportRate;
                    UserValues.enhancementMode = enhancement.ToString();
                    enhancementText.text = "Enhancement Mode: SupportRate";
                    sliderText.text = "Support Rate:";
                    UserValues.supportRate = 0;
                    supportRateSlider.value = UserValues.supportRate;
                    supportRateSlider.gameObject.SetActive(true);
                    break;
        }
    }

    //Enable / Disable the debug mode
    public void SetDebugMode(SteamVR_Action fromAction, SteamVR_Input_Sources fromSource)
    {
        debugMode = !debugMode;
        if(debugMode)
        {
            debugText.text= "Blue Ball: ON";
        }
        else
        {
            debugText.text = "Blue Ball: OFF";
        }
    }

    //Check if any key is pressed
    public void CheckKeyPressed()
    {
        if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            IncreaseSupportRate(increaseSupportRate,SteamVR_Input_Sources.Any);
            Debug.Log("SUPPORT RATE = " + UserValues.supportRate);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            DecreaseSupportRate(decreaseSupportRate, SteamVR_Input_Sources.Any);
            Debug.Log("SUPPORT RATE = " + UserValues.supportRate);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            ChangeEnhancementMode(changeEnhancementMode, SteamVR_Input_Sources.Any);
            Debug.Log("ENHANCEMENT MODE = " + UserValues.enhancementMode);
        }
    }

    public void SupportRateUpdate()
    {
        UserValues.supportRate = ((int)supportRateSlider.value);
    }
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------- Hand-Ball interaction functions --------------------------------------------------------------
    //Grab the ball by FixedJoint to connect ball to hand FATTO
    private void GrabObject()
    {
        objectInHand = collidingObject;
        collidingObject = null;
        var joint = AddFixedJoint();
        joint.connectedBody = objectInHand.GetComponent<Rigidbody>();
    }

    //Add FixedJoint with great breakForce and breakTorque values for avoiding involuntary releases FATTO
    private FixedJoint AddFixedJoint()
    {
        FixedJoint fx = gameObject.AddComponent<FixedJoint>();
        fx.breakForce = 20000;
        fx.breakTorque = 20000;
        return fx;
    }

    //When the ball is grabbed FATTO
    public void OnTriggerEnter(Collider other)
    {
        SetCollidingObject(other);
    }

    //Set the grabbed object (the ball) as a colliding object FATTO
    private void SetCollidingObject(Collider col)
    {
        if (collidingObject || !col.GetComponent<Rigidbody>())
        {
            return;
        }
        collidingObject = col.gameObject;
    }

    //When the ball is released FATTO
    public void OnTriggerExit(Collider other)
    {
        if (!collidingObject)
        {
            return;
        }
        collidingObject = null;
    }

    //Transfer position, velocity and angular velocity from source to target
    public void TransferProperties(GameObject source, GameObject target)
    {
        //Get values
        Vector3 sourceVelocity = source.GetComponent<VelocityEstimator>().GetVelocityEstimate();
        Vector3 sourceAngularVelocity = source.GetComponent<VelocityEstimator>().GetAngularVelocityEstimate();
        Vector3 sourcePosition = source.transform.position;

        //Set values
        target.GetComponent<Rigidbody>().velocity = sourceVelocity;
        target.GetComponent<Rigidbody>().angularVelocity = sourceAngularVelocity;
        target.transform.position = sourcePosition;

        //Update CSV Dict
        csvDict["Original velocity X"] = sourceVelocity.x.ToString();
        csvDict["Original velocity Y"] = sourceVelocity.y.ToString();
        csvDict["Original velocity Z"] = sourceVelocity.z.ToString();
        csvDict["Original velocity magnitude"] = sourceVelocity.magnitude.ToString();
        csvDict["Angular velocity X"] = sourceAngularVelocity.x.ToString();
        csvDict["Angular velocity Y"] = sourceAngularVelocity.y.ToString();
        csvDict["Angular velocity Z"] = sourceAngularVelocity.z.ToString();
    }

    //Release the ball
    private void ReleaseObject()
    {
        if (GetComponent<FixedJoint>())
        {
            //Remove the FixedJoint to release the ball
            GetComponent<FixedJoint>().connectedBody = null;
            Destroy(GetComponent<FixedJoint>());

            //Transfer position, velocity and angular velocity from hand to ball and debug ball
            TransferProperties(hand, ball);
            if (debugMode)
            {
                debugBall.GetComponent<MeshRenderer>().enabled = true;
                TransferProperties(hand,debugBall);
            }
            UserValues.ballIsFlying = true;
            UserValues.ballIsGrabbed = false;
            



            //Update CSV Dict
            csvDict["Elapsed time"] = elapsedTime.ToString();
            csvDict["Target mode"] = UserValues.targetChoice.ToString();
            csvDict["Support rate"] = UserValues.supportRate.ToString();
            csvDict["Launch counter"] = launchNr.ToString();
            csvDict["Enhancement mode"] = enhancementModeCounter.ToString();
            csvDict["Ball initial position X"] = ball.transform.position.x.ToString();
            csvDict["Ball initial position Y"] = ball.transform.position.y.ToString();
            csvDict["Ball initial position Z"] = ball.transform.position.z.ToString();
            csvDict["Target position X"] = target.transform.position.x.ToString();
            csvDict["Target position Y"] = target.transform.position.y.ToString();
            csvDict["Target position Z"] = target.transform.position.z.ToString();
            csvDict["Target rotation X"] = target.transform.rotation.eulerAngles.x.ToString();
            csvDict["Target rotation Y"] = target.transform.rotation.eulerAngles.y.ToString();
            csvDict["Target rotation Z"] = target.transform.rotation.eulerAngles.z.ToString();
            csvDict["Shoulder position X"] = shoulder.transform.position.x.ToString();
            csvDict["Shoulder position Y"] = shoulder.transform.position.y.ToString();
            csvDict["Shoulder position Z"] = shoulder.transform.position.z.ToString();

            ImprovingShotDirection(ball, target);
            ImprovingShotVelocity(ball,target);
            WritingCSVFile();
        }
        objectInHand = null;
    }
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------- CSV functions -----------------------------------------------------------------------
    //Create the CSV file and the Data dictionary
    private Dictionary<string, string> CreateCSV()
    {
        Dictionary<string, string> dict = new()
        {
            { "Launch counter", "0" },
            { "Target mode", "0" },
            { "Support rate", "0" },
            { "Ball initial position X", "0" },
            { "Ball initial position Y", "0" },
            { "Ball initial position Z", "0" },
            { "Target position X", "0" },
            { "Target position Y", "0" },
            { "Target position Z", "0" },
            { "Target rotation X", "0" },
            { "Target rotation Y", "0" },
            { "Target rotation Z", "0" },
            { "Shoulder position X", "0" },
            { "Shoulder position Y", "0" },
            { "Shoulder position Z", "0" },
            { "Original velocity X", "0" },
            { "Original velocity Y", "0" },
            { "Original velocity Z", "0" },
            { "Velocity force X", "0" },
            { "Velocity force Y", "0" },
            { "Velocity force Z", "0" },
            { "Direction force X", "0" },
            { "Direction force Y", "0" },
            { "Direction force Z", "0" },
            { "Original velocity magnitude", "0" },
            { "Modified velocity X", "0" },
            { "Modified velocity Y", "0" },
            { "Modified velocity Z", "0" },
            { "Modified velocity magnitude", "0" },
            { "Angular velocity X", "0" },
            { "Angular velocity Y", "0" },
            { "Angular velocity Z", "0" },
            { "Original angle of error", "0" },
            { "Amount of angle improvement", "0" },
            { "Modified angle of error", "0" },
            { "Elapsed time", "0" },
            { "Enhancement mode", "0" } 
        };

        string stringToWrite = "";
        foreach (string str in dict.Keys)
        {
            stringToWrite += (str + ",");
        }
        stringToWrite = stringToWrite.Remove(stringToWrite.Length - 1);

        _ = new FileInfo(CSVpath);
        TextWriter tw = new StreamWriter(CSVpath, true);
        tw.WriteLine(stringToWrite);
        tw.Close();

        return dict;
    }

    //Updating the CSV Data every launch
    private void WritingCSVFile()
    {
        TextWriter tw = new StreamWriter(CSVpath, true);

        string stringToWrite = "";
        foreach (string value in csvDict.Values)
        {
            stringToWrite += (value + ",");
        }
        stringToWrite = stringToWrite.Remove(stringToWrite.Length - 1);

        tw.WriteLine(stringToWrite);
        tw.Close();
    }
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------
}