using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitChecker : MonoBehaviour
{
    [SerializeField]
    private GameObject breakEffect;
    private Dictionary<int, int> hitDict;
    private int launchNumber = 0;

    void Start()
    {
        hitDict= new Dictionary<int, int>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Ball" && StaticValues.floorHit == false)
        {
            GenerateEffect();
            Destroy(gameObject);
        }
    }

    void GenerateEffect()
    {
        GameObject effect = Instantiate(breakEffect) as GameObject;
        effect.transform.position = gameObject.transform.position;
    }

    public void SetLaunchNumber(int nr)
    {
        launchNumber = nr;
    }
}
