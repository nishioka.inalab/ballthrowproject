using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;//加速度計測用

public class AngularAcceleration : MonoBehaviour
{
    //速度関連スクリプト
    private VelocityEstimator velocityEstimator;
    //加速度格納用
    [HideInInspector]
    public Vector3 angularAcceleration;
    //前フレームの速度保持用
    private Vector3 oldAngularVelocity;
    void Start()
    {
        //速度計測用スクリプトを取得
        velocityEstimator = this.GetComponent<VelocityEstimator>();
    }

    // Update is called once per frame
    void Update()
    {
        //速度から加速度を算出
        angularAcceleration = (velocityEstimator.GetAngularVelocityEstimate() - oldAngularVelocity) / Time.deltaTime;
        //前フレームの速度を記録
        oldAngularVelocity = velocityEstimator.GetAngularVelocityEstimate();
    }
}
