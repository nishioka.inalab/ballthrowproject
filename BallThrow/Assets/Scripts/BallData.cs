using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class BallData : MonoBehaviour
{
    [SerializeField]
    private GameObject target;
    [SerializeField]
    private GameObject wall;
    [SerializeField]
    private SupportRateManager supportRateManager;
    private static readonly string dataSeparator = ",";
    private readonly List<string> dataList = new();
    private int previousLaunch = 0;
    private static readonly string[] dataHeaders = new string[]
    {
        "Launch number",
        "Target hit",
        "Error",
        "Drop point X",
        "Drop point Y"
    };

    void Start()
    {
        System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
        customCulture.NumberFormat.NumberDecimalSeparator = ".";
        System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

        UserValues.ballIsFlying = false;


    }

    /*private void OnCollisionEnter(Collision collision)
    {
        if (UserValues.ballIsFlying)
        {
            UserValues.ballIsFlying = false;
            if (UserValues.launchCounter == (previousLaunch + 1))
            {
                previousLaunch++;
                dataList.Add(UserValues.launchCounter.ToString());
                float currentError;
                Vector3 dropPoint = Vector3.zero;
                Vector3 targetPoint = Vector3.zero;
                if (UserValues.targetChoice == "Ground")
                {
                    if (collision.gameObject == target)
                    {
                        dataList.Add(true.ToString());
                        Debug.Log(collision.gameObject.name);

                        if(UserValues.enhancementMode == "SupportRate" && UserValues.adjustMode == "Dynamic")
                        {
                            supportRateManager.CountSuccess();
                        }
                    }
                    else
                    {
                        dataList.Add(false.ToString());
                        Debug.Log(collision.gameObject.name);

                        if (UserValues.enhancementMode == "SupportRate" && UserValues.adjustMode == "Dynamic")
                        {
                            supportRateManager.CountFailure();
                        }
                    }

                    dropPoint.Set(collision.contacts[0].point.x, 0, collision.contacts[0].point.z);
                    targetPoint.Set(target.transform.position.x, 0, target.transform.position.z);
                    currentError = Vector3.Distance(dropPoint, targetPoint);

                    if (currentError < 0.075)
                    {
                        currentError = 0f;
                    }

                    dataList.Add(currentError.ToString());
                    Vector2 scatterCoordinates = GetScatterCoordinates(dropPoint, targetPoint);
                    dataList.Add(scatterCoordinates.x.ToString());
                    dataList.Add(scatterCoordinates.y.ToString());
                }
                else if (UserValues.targetChoice == "Air")
                {
                    if (collision.gameObject == target)
                    {
                        dataList.Add(true.ToString());

                        if (UserValues.enhancementMode == "SupportRate" && UserValues.adjustMode == "Dynamic")
                        {
                            supportRateManager.CountSuccess();
                        }

                        dropPoint.Set(0, collision.contacts[0].point.y, 0);
                        targetPoint.Set(0, target.transform.position.y, 0);
                        currentError = Vector3.Distance(dropPoint, targetPoint);
                        

                        if (currentError < 0.075)
                        {
                            currentError = 0f;
                        }

                        dataList.Add(currentError.ToString());
                    }
                    else
                    {
                        dataList.Add(false.ToString());

                        if (UserValues.enhancementMode == "SupportRate" && UserValues.adjustMode == "Dynamic")
                        {
                            supportRateManager.CountFailure();
                        }

                        dropPoint = collision.contacts[0].point;
                        targetPoint = target.transform.position;
                        currentError = Vector3.Distance(dropPoint, targetPoint);

                        if (currentError < 0.075)
                        {
                            currentError = 0f;
                        }
                        dataList.Add(currentError.ToString());
                    }

                    dataList.Add("0");
                    dataList.Add("0");

                }
                AppendData(dataList.ToArray());
                dataList.Clear();
            }
        }
    }*/

    private Vector2 GetScatterCoordinates(Vector3 dropPoint, Vector3 targetPoint)
    {
        float xCoord = dropPoint.x - targetPoint.x;
        float yCoord = dropPoint.z - targetPoint.z;
        return new Vector2(xCoord, yCoord);
    }

    private void VerifyDirectory()
    {
        string dir = GetDirectoryPath();
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
    }

    private void VerifyFile()
    {
        string file = GetFilePath();
        if (!File.Exists(file))
        {
            CreateDataFile();
        }
    }

    private string GetDirectoryPath()
    {
        return Application.dataPath + UserValues.pathGeneral;
    }

    private string GetFilePath()
    {
        return GetDirectoryPath() + "/" + gameObject.name + "Data_" + UserValues.userName + ".csv";
    }

    public void AppendData(string[] dataString)
    {
        VerifyDirectory();
        VerifyFile();

        TextWriter tw = new StreamWriter(GetFilePath(), true);

        string stringToWrite = "";
        foreach (string str in dataString)
        {
            if (stringToWrite != "")
            {
                stringToWrite += dataSeparator;
            }
            stringToWrite += str;
        }

        tw.WriteLine(stringToWrite);
        tw.Close();
    }

    public void CreateDataFile()
    {
        VerifyDirectory();
        string stringToWrite = "";
        foreach (string str in dataHeaders)
        {
            if (stringToWrite != "")
            {
                stringToWrite += dataSeparator;
            }
            stringToWrite += str;
        }
        _ = new FileInfo(GetFilePath());
        TextWriter tw = new StreamWriter(GetFilePath(), true);
        tw.WriteLine(stringToWrite);
        tw.Close();
    }
}
