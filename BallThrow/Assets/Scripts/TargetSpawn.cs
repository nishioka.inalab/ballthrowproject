
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSpawn : MonoBehaviour
{
    //敵プレハブ
    [SerializeField]
    private GameObject target;
    //敵生成時間間隔
    [SerializeField]
    private float maxInterval = 5f;
    [SerializeField]
    private float minInterval = 3f;

    private float interval = 5f;
    //経過時間
    private float time = 0f;

    //出現位置設定用
    //X座標の最小値
    [SerializeField]
    private float xMinPosition = -1f;
    //X座標の最大値
    [SerializeField]
    private float xMaxPosition = 1f;
    //Y座標の最小値
    [SerializeField]
    private float yMinPosition = 0.5f;
    //Y座標の最大値
    [SerializeField]
    private float yMaxPosition = 2f;
    //Z座標の最小値
    [SerializeField]
    private float zMinPosition = 0.5f;
    //Z座標の最大値
    [SerializeField]
    private float zMaxPosition = 1.5f;

    void Start()
    {
        interval = GetRandomTime();
    }
    // Update is called once per frame
    void Update()
    {
        //時間計測
        time += Time.deltaTime;

        //経過時間が生成時間になったとき(生成時間より大きくなったとき)
        if (time > interval)
        {
            //enemyをインスタンス化する(生成する)
            GameObject enemy = Instantiate(target);
            //生成した敵の位置をランダムに設定する
            enemy.transform.position = GetRandomPosition();
            //経過時間を初期化して再度時間計測を始める
            time = 0f;
            //次の出現時間の再設定
            interval = GetRandomTime();
        }
    }

    //ランダムな時間を生成する関数
    private float GetRandomTime()
    {
        return Random.Range(minInterval, maxInterval);
    }

    //ランダムな位置を生成する関数
    private Vector3 GetRandomPosition()
    {
        //それぞれの座標をランダムに生成する
        float x = Random.Range(xMinPosition, xMaxPosition);
        float y = Random.Range(yMinPosition, yMaxPosition);
        float z = Random.Range(zMinPosition, zMaxPosition);

        //Vector3型のPositionを返す
        return new Vector3(x, y, z);
    }
}