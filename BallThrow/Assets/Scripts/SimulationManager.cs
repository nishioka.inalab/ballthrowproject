using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Transactions;
using System;
using Unity.VisualScripting;
using UnityEngine.UI;
using static BallHandThrow1;

public class SimulationManager : MonoBehaviour
{

    string CSVpath = "";
    Dictionary<string,float> launchDict= new Dictionary<string,float>();
    bool flyingBall = false;
    Vector3 velocityForce, directionForce = Vector3.zero;

    [SerializeField]
    GameObject ball, debugBall, target, shoulder;
    [SerializeField]
    Text launchNrText, SupportRateText, EnhancementModeText;

    // Start is called before the first frame update
    void Start()
    {
        CSVpath = Application.dataPath + "/CSV Simulation/a.csv";
        System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
        customCulture.NumberFormat.NumberDecimalSeparator = ".";

        System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

        StartCoroutine(ReadCSV());
    }

    private void FixedUpdate()
    {
        if (flyingBall)
        {
            ball.GetComponent<Rigidbody>().AddForce(velocityForce, ForceMode.Force);
            ball.GetComponent<Rigidbody>().AddForce(directionForce, ForceMode.Force);
        }
    }


    IEnumerator ReadCSV()
    {
        float prevTime = 0;
        int rowCounter = 0;
        string[] headWords = null;
        StreamReader tr = new StreamReader(CSVpath);
        while (!tr.EndOfStream)
        {
            rowCounter++;
            string currentRow = tr.ReadLine();
            
            if(rowCounter == 1)
            {
                headWords = currentRow.Split(",");
                foreach (var word in headWords)
                {
                    launchDict.Add(word, 0);
                }
            }else
            {
                string[] values = currentRow.Split(",");

                for(int i = 0; i < headWords.Length; i++)
                {
                    launchDict[headWords[i]] = float.Parse(values[i]);
                }



                yield return new WaitForSeconds(launchDict["Elapsed time"] - prevTime);
                prevTime = launchDict["Elapsed time"];
                flyingBall = false;
                velocityForce = Vector3.zero;
                directionForce = Vector3.zero;

                launchNrText.text = "Launch nr: " + launchDict["Launch counter"];
                switch (launchDict["Enhancement mode"])
                {
                    case 0:
                        EnhancementModeText.text = "Enhancement mode: None";
                        SupportRateText.text = "";
                        break;
                    case 1:
                        EnhancementModeText.text = "Enhancement mode: OneTime";
                        SupportRateText.text = "Direction correction: " + launchDict["Support rate"] + "%";
                        break;
                    case 2:
                        EnhancementModeText.text = "Enhancement mode: ContinuousForce";
                        SupportRateText.text = "";
                        flyingBall = true;
                        break;
                    case 3:
                        EnhancementModeText.text = "Enhancement mode: SupportRate";
                        SupportRateText.text = "Support Rate: " + launchDict["Support rate"] + "%";
                        break;
                }
                Simulation();
            }

            
        }
        tr.Close();
        yield return null;
    }

    private void Simulation()
    {
        Vector3 ballInitialPos= new Vector3(launchDict["Ball initial position X"], launchDict["Ball initial position Y"], launchDict["Ball initial position Z"]);
        Vector3 shoulderInitialPos = new Vector3(launchDict["Shoulder position X"], launchDict["Shoulder position Y"], launchDict["Shoulder position Z"]);
        Vector3 targetInitialPos = new Vector3(launchDict["Target position X"], launchDict["Target position Y"], launchDict["Target position Z"]);
        Vector3 originalVelocity = new Vector3(launchDict["Original velocity X"], launchDict["Original velocity Y"], launchDict["Original velocity Z"]);
        Vector3 modifiedVelocity = new Vector3(launchDict["Modified velocity X"], launchDict["Modified velocity Y"], launchDict["Modified velocity Z"]);
        Vector3 angularVelocity = new Vector3(launchDict["Angular velocity X"], launchDict["Angular velocity Y"], launchDict["Angular velocity Z"]);
        velocityForce.Set(launchDict["Velocity force X"], launchDict["Velocity force Y"], launchDict["Velocity force Z"]);
        directionForce.Set(launchDict["Direction force X"], launchDict["Direction force Y"], launchDict["Direction force Z"]);

        ball.transform.position = ballInitialPos;
        shoulder.transform.position = shoulderInitialPos;
        target.transform.position = targetInitialPos;
        debugBall.transform.position = ballInitialPos;

        


        debugBall.GetComponent<Rigidbody>().velocity = originalVelocity;
        debugBall.GetComponent<Rigidbody>().angularVelocity = angularVelocity;
        ball.GetComponent<Rigidbody>().velocity = modifiedVelocity;
        ball.GetComponent<Rigidbody>().angularVelocity = angularVelocity;
        
    }
}
