using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class SimulationManager_V2 : MonoBehaviour
{
    [SerializeField] private LineRenderer GazeRayRenderer;
    private static string dataDirectoryName = "CSV Simulation";
    private static string dataSeparator = ",";
    public int LengthOfRay = 25;
    Dictionary<string,string> data = new Dictionary<string,string>();
    [SerializeField]
    private GameObject ball, debugBall, camera, target, hand, wall;
    [SerializeField]
    private Text launchNrText;
    private int launchNr = 0;
    // Start is called before the first frame update
    void Start()
    {
        System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
        customCulture.NumberFormat.NumberDecimalSeparator = ".";

        System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

        InitializeScene();
        StartCoroutine(ReadCSV());
    }

    IEnumerator ReadCSV()
    {
        float prevTime = 0;
        int rowCounter = 0;
        StreamReader tr = new StreamReader(GetFilePath("FBF"));
        while (!tr.EndOfStream)
        {
            rowCounter++;
            string currentRow = tr.ReadLine();

            if (rowCounter == 1)
            {
                //Riga 1 non fare nulla
            }
            else
            {
                string[] values = currentRow.Split(dataSeparator);

                
                yield return new WaitForSeconds(float.Parse(values[1]) - prevTime);
                prevTime = float.Parse(values[1]);

                if (int.Parse(values[0]) != launchNr)
                {
                    launchNrText.text = "Launch nr:" + values[0];
                    launchNr = int.Parse(values[0]);
                }
                
                Vector3 cameraPosition = new Vector3(float.Parse(values[3]), float.Parse(values[4]), float.Parse(values[5]));
                Vector3 cameraRotation = new Vector3(float.Parse(values[6]), float.Parse(values[7]), float.Parse(values[8]));
                Vector3 gazeVector = new Vector3(float.Parse(values[9]), float.Parse(values[10]), float.Parse(values[11]));
                Vector3 ballPosition = new Vector3(float.Parse(values[22]), float.Parse(values[23]), float.Parse(values[24]));
                Vector3 debugBallPosition = new Vector3(float.Parse(values[28]), float.Parse(values[29]), float.Parse(values[30]));
                Vector3 handPosition = new Vector3(float.Parse(values[34]), float.Parse(values[35]), float.Parse(values[36]));
                Vector3 handRotation = new Vector3(float.Parse(values[37]), float.Parse(values[38]), float.Parse(values[39]));

                GazeRayRenderer.SetPosition(0, camera.transform.position - camera.transform.up * 0.05f);
                GazeRayRenderer.SetPosition(1, camera.transform.position + gazeVector * LengthOfRay);
                ball.transform.position = ballPosition;
                debugBall.transform.position = debugBallPosition;

                hand.transform.position = handPosition;
                hand.transform.eulerAngles = handRotation;
                camera.transform.position = cameraPosition;
                camera.transform.eulerAngles = cameraRotation;

            }
        }
        tr.Close();
        yield return null;
    }

    private void InitializeScene()
    {
        int rowCounter = 0;
        string[] headWords = null;
        StreamReader tr = new StreamReader(GetFilePath("GENERAL"));
        while (rowCounter < 2)
        {
            rowCounter++;
            string currentRow = tr.ReadLine();

            if (rowCounter == 1)
            {
                headWords = currentRow.Split(",");
                foreach (var word in headWords)
                {
                    data.Add(word, "");
                }
            }
            else
            {
                string[] values = currentRow.Split(dataSeparator);
                for (int i = 0; i < headWords.Length; i++)
                {
                    data[headWords[i]] = values[i];
                }
            }
        }
        tr.Close();

        if (data["Target mode"] == "Air")
        {
            wall.SetActive(true);
        }

        Vector3 targetPosition = new(float.Parse(data["Target position X"]), float.Parse(data["Target position Y"]), float.Parse(data["Target position Z"]));
        Vector3 targetRotation = new(float.Parse(data["Target rotation X"]), float.Parse(data["Target rotation Y"]), float.Parse(data["Target rotation Z"]));

        target.transform.position = targetPosition;
        target.transform.eulerAngles = targetRotation;
    }

    static string GetDirectoryPath()
    {
        return Application.dataPath + "/" + dataDirectoryName;
    }

    static string GetFilePath(string fileName)
    {
        return GetDirectoryPath() + "/" + fileName + ".csv";
    }
}
